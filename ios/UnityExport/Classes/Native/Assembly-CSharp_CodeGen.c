﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 <CallbackTest>j__TPar <>f__AnonymousType0`1::get_CallbackTest()
// 0x00000002 System.Void <>f__AnonymousType0`1::.ctor(<CallbackTest>j__TPar)
// 0x00000003 System.Boolean <>f__AnonymousType0`1::Equals(System.Object)
// 0x00000004 System.Int32 <>f__AnonymousType0`1::GetHashCode()
// 0x00000005 System.String <>f__AnonymousType0`1::ToString()
// 0x00000006 <id>j__TPar <>f__AnonymousType1`4::get_id()
// 0x00000007 <seq>j__TPar <>f__AnonymousType1`4::get_seq()
// 0x00000008 <name>j__TPar <>f__AnonymousType1`4::get_name()
// 0x00000009 <data>j__TPar <>f__AnonymousType1`4::get_data()
// 0x0000000A System.Void <>f__AnonymousType1`4::.ctor(<id>j__TPar,<seq>j__TPar,<name>j__TPar,<data>j__TPar)
// 0x0000000B System.Boolean <>f__AnonymousType1`4::Equals(System.Object)
// 0x0000000C System.Int32 <>f__AnonymousType1`4::GetHashCode()
// 0x0000000D System.String <>f__AnonymousType1`4::ToString()
// 0x0000000E System.Void Rotate::Awake()
extern void Rotate_Awake_m98272D8BD36400F05C40890A9F7E6AAA35AB7515 (void);
// 0x0000000F System.Void Rotate::onDestroy()
extern void Rotate_onDestroy_m36332C66DECAA5FF23044FB5D6873B7E38AA129A (void);
// 0x00000010 System.Void Rotate::onMessage(MessageHandler)
extern void Rotate_onMessage_m2EB175A859194F332EBA43D1F532207B84EA915C (void);
// 0x00000011 System.Void Rotate::OnMouseDown()
extern void Rotate_OnMouseDown_mA2F15CCF41D63C626D70DA7338B4146EC13B9988 (void);
// 0x00000012 System.Void Rotate::Update()
extern void Rotate_Update_m7EECA0DFF07C6BCA773478F66A949EE038D28330 (void);
// 0x00000013 System.Void Rotate::.ctor()
extern void Rotate__ctor_mC70374AA84BC2CA04CA039D1B56F5AF1B95D42CF (void);
// 0x00000014 MessageHandler MessageHandler::Deserialize(System.String)
extern void MessageHandler_Deserialize_mA9693BA8D2C9FD955FEA9DD8E48D55E6584BB8DE (void);
// 0x00000015 T MessageHandler::getData()
// 0x00000016 System.Void MessageHandler::.ctor(System.Int32,System.String,System.String,Newtonsoft.Json.Linq.JToken)
extern void MessageHandler__ctor_mD413687341FACD003432FE926B76CC1DE2E78EFC (void);
// 0x00000017 System.Void MessageHandler::send(System.Object)
extern void MessageHandler_send_m0BF4951456C3CA50EEF785A982A6F10D92BD0C5E (void);
// 0x00000018 System.Void UnityMessage::.ctor()
extern void UnityMessage__ctor_m76E68BAF631BE07953E806CA26C480BF23B2ED45 (void);
// 0x00000019 System.Void UnityMessageManager::onUnityMessage(System.String)
extern void UnityMessageManager_onUnityMessage_mC3031C4585567E720F536DB4EE28B0C20AB8A7C4 (void);
// 0x0000001A System.Int32 UnityMessageManager::generateId()
extern void UnityMessageManager_generateId_m39045BEBA86C4467AD5549DFA0DDCDF9A7A9CB89 (void);
// 0x0000001B UnityMessageManager UnityMessageManager::get_Instance()
extern void UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16 (void);
// 0x0000001C System.Void UnityMessageManager::set_Instance(UnityMessageManager)
extern void UnityMessageManager_set_Instance_m25A5E4940172A7950BA2EED339C161A0C74B05AE (void);
// 0x0000001D System.Void UnityMessageManager::add_OnMessage(UnityMessageManager_MessageDelegate)
extern void UnityMessageManager_add_OnMessage_m7B623FDD1EB187BC9593C933D5E01766FC8DB344 (void);
// 0x0000001E System.Void UnityMessageManager::remove_OnMessage(UnityMessageManager_MessageDelegate)
extern void UnityMessageManager_remove_OnMessage_mE747C20C6838E183F2C60960D2C697D7997588F4 (void);
// 0x0000001F System.Void UnityMessageManager::add_OnRNMessage(UnityMessageManager_MessageHandlerDelegate)
extern void UnityMessageManager_add_OnRNMessage_mD1D6F59672DB48353711FBD7CC5A1C5573FF33FA (void);
// 0x00000020 System.Void UnityMessageManager::remove_OnRNMessage(UnityMessageManager_MessageHandlerDelegate)
extern void UnityMessageManager_remove_OnRNMessage_m12B8512E35DC119F3800C8C8A05442F9C0A19B5E (void);
// 0x00000021 System.Void UnityMessageManager::.cctor()
extern void UnityMessageManager__cctor_mDD0387729BD393CF289CB737BC70BF82CFAC885B (void);
// 0x00000022 System.Void UnityMessageManager::Awake()
extern void UnityMessageManager_Awake_mCBF7274D38E90447257210355EEBED184FF0C035 (void);
// 0x00000023 System.Void UnityMessageManager::SendMessageToRN(System.String)
extern void UnityMessageManager_SendMessageToRN_m7FA689191F69A7BD1CAF2218D61EE8BCE156B250 (void);
// 0x00000024 System.Void UnityMessageManager::SendMessageToRN(UnityMessage)
extern void UnityMessageManager_SendMessageToRN_m810379DB93ACA8FB4F37914CA49BA677BDE94E3E (void);
// 0x00000025 System.Void UnityMessageManager::onMessage(System.String)
extern void UnityMessageManager_onMessage_mD1704644A461EFD311809BB58F82D36898713485 (void);
// 0x00000026 System.Void UnityMessageManager::onRNMessage(System.String)
extern void UnityMessageManager_onRNMessage_m47BA244DB7A9E9A2486007E2F5217A03FFFC1AF8 (void);
// 0x00000027 System.Void UnityMessageManager::.ctor()
extern void UnityMessageManager__ctor_mFB3651A2237E489ED8746D5B8FC5B5AB95FE7CE7 (void);
// 0x00000028 System.Void Rotate_<>c::.cctor()
extern void U3CU3Ec__cctor_mDA82C2EF6063BB47B299296272C713873EB938C5 (void);
// 0x00000029 System.Void Rotate_<>c::.ctor()
extern void U3CU3Ec__ctor_mBFB4BD35AF855B60B3202619E831D4FB394CFC48 (void);
// 0x0000002A System.Void Rotate_<>c::<OnMouseDown>b__4_0(System.Object)
extern void U3CU3Ec_U3COnMouseDownU3Eb__4_0_m174DA0084BF158C76305355492BE3C1FDDAAB0B1 (void);
// 0x0000002B System.Void UnityMessageManager_MessageDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageDelegate__ctor_m3EA48B06536871149F1A43C905528166D49F48C8 (void);
// 0x0000002C System.Void UnityMessageManager_MessageDelegate::Invoke(System.String)
extern void MessageDelegate_Invoke_m2D199E1633D3EFBEA727164B8A3F7888E6A4F801 (void);
// 0x0000002D System.IAsyncResult UnityMessageManager_MessageDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void MessageDelegate_BeginInvoke_m89C256246D24A40498952C82CDD615B3507E4BA4 (void);
// 0x0000002E System.Void UnityMessageManager_MessageDelegate::EndInvoke(System.IAsyncResult)
extern void MessageDelegate_EndInvoke_m778326112BD3D26CB9DE0FFC335E9A1A9353BC35 (void);
// 0x0000002F System.Void UnityMessageManager_MessageHandlerDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageHandlerDelegate__ctor_m58386047CB442F144728F0A568B26FDDDB0EE6EA (void);
// 0x00000030 System.Void UnityMessageManager_MessageHandlerDelegate::Invoke(MessageHandler)
extern void MessageHandlerDelegate_Invoke_mF982647CDFA5782B9ABAF328FF4070492A364B43 (void);
// 0x00000031 System.IAsyncResult UnityMessageManager_MessageHandlerDelegate::BeginInvoke(MessageHandler,System.AsyncCallback,System.Object)
extern void MessageHandlerDelegate_BeginInvoke_mFDBB4C951CAC28A4F0E08666050BB3F266FA927E (void);
// 0x00000032 System.Void UnityMessageManager_MessageHandlerDelegate::EndInvoke(System.IAsyncResult)
extern void MessageHandlerDelegate_EndInvoke_m9BC3850452E21DEBBF15C7598D811D4D744633A6 (void);
static Il2CppMethodPointer s_methodPointers[50] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Rotate_Awake_m98272D8BD36400F05C40890A9F7E6AAA35AB7515,
	Rotate_onDestroy_m36332C66DECAA5FF23044FB5D6873B7E38AA129A,
	Rotate_onMessage_m2EB175A859194F332EBA43D1F532207B84EA915C,
	Rotate_OnMouseDown_mA2F15CCF41D63C626D70DA7338B4146EC13B9988,
	Rotate_Update_m7EECA0DFF07C6BCA773478F66A949EE038D28330,
	Rotate__ctor_mC70374AA84BC2CA04CA039D1B56F5AF1B95D42CF,
	MessageHandler_Deserialize_mA9693BA8D2C9FD955FEA9DD8E48D55E6584BB8DE,
	NULL,
	MessageHandler__ctor_mD413687341FACD003432FE926B76CC1DE2E78EFC,
	MessageHandler_send_m0BF4951456C3CA50EEF785A982A6F10D92BD0C5E,
	UnityMessage__ctor_m76E68BAF631BE07953E806CA26C480BF23B2ED45,
	UnityMessageManager_onUnityMessage_mC3031C4585567E720F536DB4EE28B0C20AB8A7C4,
	UnityMessageManager_generateId_m39045BEBA86C4467AD5549DFA0DDCDF9A7A9CB89,
	UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16,
	UnityMessageManager_set_Instance_m25A5E4940172A7950BA2EED339C161A0C74B05AE,
	UnityMessageManager_add_OnMessage_m7B623FDD1EB187BC9593C933D5E01766FC8DB344,
	UnityMessageManager_remove_OnMessage_mE747C20C6838E183F2C60960D2C697D7997588F4,
	UnityMessageManager_add_OnRNMessage_mD1D6F59672DB48353711FBD7CC5A1C5573FF33FA,
	UnityMessageManager_remove_OnRNMessage_m12B8512E35DC119F3800C8C8A05442F9C0A19B5E,
	UnityMessageManager__cctor_mDD0387729BD393CF289CB737BC70BF82CFAC885B,
	UnityMessageManager_Awake_mCBF7274D38E90447257210355EEBED184FF0C035,
	UnityMessageManager_SendMessageToRN_m7FA689191F69A7BD1CAF2218D61EE8BCE156B250,
	UnityMessageManager_SendMessageToRN_m810379DB93ACA8FB4F37914CA49BA677BDE94E3E,
	UnityMessageManager_onMessage_mD1704644A461EFD311809BB58F82D36898713485,
	UnityMessageManager_onRNMessage_m47BA244DB7A9E9A2486007E2F5217A03FFFC1AF8,
	UnityMessageManager__ctor_mFB3651A2237E489ED8746D5B8FC5B5AB95FE7CE7,
	U3CU3Ec__cctor_mDA82C2EF6063BB47B299296272C713873EB938C5,
	U3CU3Ec__ctor_mBFB4BD35AF855B60B3202619E831D4FB394CFC48,
	U3CU3Ec_U3COnMouseDownU3Eb__4_0_m174DA0084BF158C76305355492BE3C1FDDAAB0B1,
	MessageDelegate__ctor_m3EA48B06536871149F1A43C905528166D49F48C8,
	MessageDelegate_Invoke_m2D199E1633D3EFBEA727164B8A3F7888E6A4F801,
	MessageDelegate_BeginInvoke_m89C256246D24A40498952C82CDD615B3507E4BA4,
	MessageDelegate_EndInvoke_m778326112BD3D26CB9DE0FFC335E9A1A9353BC35,
	MessageHandlerDelegate__ctor_m58386047CB442F144728F0A568B26FDDDB0EE6EA,
	MessageHandlerDelegate_Invoke_mF982647CDFA5782B9ABAF328FF4070492A364B43,
	MessageHandlerDelegate_BeginInvoke_mFDBB4C951CAC28A4F0E08666050BB3F266FA927E,
	MessageHandlerDelegate_EndInvoke_m9BC3850452E21DEBBF15C7598D811D4D744633A6,
};
static const int32_t s_InvokerIndices[50] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	26,
	23,
	23,
	23,
	0,
	-1,
	40,
	26,
	23,
	111,
	547,
	4,
	111,
	26,
	26,
	26,
	26,
	3,
	23,
	26,
	26,
	26,
	26,
	23,
	3,
	23,
	26,
	163,
	26,
	166,
	26,
	163,
	26,
	166,
	26,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x02000002, { 0, 6 } },
	{ 0x02000003, { 6, 21 } },
	{ 0x06000015, { 27, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[28] = 
{
	{ (Il2CppRGCTXDataType)2, 12092 },
	{ (Il2CppRGCTXDataType)3, 6403 },
	{ (Il2CppRGCTXDataType)2, 12093 },
	{ (Il2CppRGCTXDataType)3, 6404 },
	{ (Il2CppRGCTXDataType)3, 6405 },
	{ (Il2CppRGCTXDataType)2, 11801 },
	{ (Il2CppRGCTXDataType)2, 12094 },
	{ (Il2CppRGCTXDataType)3, 6406 },
	{ (Il2CppRGCTXDataType)2, 12095 },
	{ (Il2CppRGCTXDataType)3, 6407 },
	{ (Il2CppRGCTXDataType)3, 6408 },
	{ (Il2CppRGCTXDataType)2, 12096 },
	{ (Il2CppRGCTXDataType)3, 6409 },
	{ (Il2CppRGCTXDataType)3, 6410 },
	{ (Il2CppRGCTXDataType)2, 12097 },
	{ (Il2CppRGCTXDataType)3, 6411 },
	{ (Il2CppRGCTXDataType)3, 6412 },
	{ (Il2CppRGCTXDataType)2, 12098 },
	{ (Il2CppRGCTXDataType)3, 6413 },
	{ (Il2CppRGCTXDataType)3, 6414 },
	{ (Il2CppRGCTXDataType)3, 6415 },
	{ (Il2CppRGCTXDataType)3, 6416 },
	{ (Il2CppRGCTXDataType)3, 6417 },
	{ (Il2CppRGCTXDataType)2, 11805 },
	{ (Il2CppRGCTXDataType)2, 11806 },
	{ (Il2CppRGCTXDataType)2, 11807 },
	{ (Il2CppRGCTXDataType)2, 11808 },
	{ (Il2CppRGCTXDataType)3, 6418 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	50,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	28,
	s_rgctxValues,
	NULL,
};
