﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 (void);
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneElement()
extern void Error_MoreThanOneElement_mD96D1249F5D42379E9417302B5F33DD99B51C863 (void);
// 0x00000004 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000005 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 (void);
// 0x00000006 System.Exception System.Linq.Error::NotSupported()
extern void Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D (void);
// 0x00000007 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000009 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x0000000A System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x0000000B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000D System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000E System.Collections.Generic.IEnumerable`1<System.Linq.IGrouping`2<TKey,TSource>> System.Linq.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000010 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::UnionIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000011 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000012 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000013 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>)
// 0x00000014 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000015 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000016 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000017 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000018 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000019 TSource System.Linq.Enumerable::LastOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001A TSource System.Linq.Enumerable::Single(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001B TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001C TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x0000001E System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001F System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000020 System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000021 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000022 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000023 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000024 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000025 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000026 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000027 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000028 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000029 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002A System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000002B System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000002C System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000002D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000002E System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000002F System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000030 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000031 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000032 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000033 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000034 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000035 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000036 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000037 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000038 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000039 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000003A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000003B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000003C System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000003D System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x0000003E System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000003F System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000040 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000041 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000042 System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000043 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000044 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000045 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000046 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000047 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000048 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000049 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x0000004A System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000004B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000004C System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x0000004D System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000004E System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x0000004F TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000050 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x00000051 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x00000052 System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__17`2::MoveNext()
// 0x00000053 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x00000054 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x00000055 TResult System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000056 System.Object System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x00000057 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000058 System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000059 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::.ctor(System.Int32)
// 0x0000005A System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::System.IDisposable.Dispose()
// 0x0000005B System.Boolean System.Linq.Enumerable_<UnionIterator>d__71`1::MoveNext()
// 0x0000005C System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::<>m__Finally1()
// 0x0000005D System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::<>m__Finally2()
// 0x0000005E TSource System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000005F System.Object System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerator.get_Current()
// 0x00000060 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000061 System.Collections.IEnumerator System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000062 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x00000063 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x00000064 System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x00000065 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x00000066 TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000067 System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x00000068 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000069 System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000006A System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x0000006B System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x0000006C System.Void System.Linq.IdentityFunction`1_<>c::.cctor()
// 0x0000006D System.Void System.Linq.IdentityFunction`1_<>c::.ctor()
// 0x0000006E TElement System.Linq.IdentityFunction`1_<>c::<get_Instance>b__1_0(TElement)
// 0x0000006F System.Linq.Lookup`2<TKey,TElement> System.Linq.Lookup`2::Create(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000070 System.Void System.Linq.Lookup`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000071 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.Lookup`2::GetEnumerator()
// 0x00000072 System.Collections.IEnumerator System.Linq.Lookup`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000073 System.Int32 System.Linq.Lookup`2::InternalGetHashCode(TKey)
// 0x00000074 System.Linq.Lookup`2_Grouping<TKey,TElement> System.Linq.Lookup`2::GetGrouping(TKey,System.Boolean)
// 0x00000075 System.Void System.Linq.Lookup`2::Resize()
// 0x00000076 System.Void System.Linq.Lookup`2_Grouping::Add(TElement)
// 0x00000077 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.Lookup`2_Grouping::GetEnumerator()
// 0x00000078 System.Collections.IEnumerator System.Linq.Lookup`2_Grouping::System.Collections.IEnumerable.GetEnumerator()
// 0x00000079 System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_Count()
// 0x0000007A System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_IsReadOnly()
// 0x0000007B System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x0000007C System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x0000007D System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Contains(TElement)
// 0x0000007E System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.CopyTo(TElement[],System.Int32)
// 0x0000007F System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x00000080 System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.IndexOf(TElement)
// 0x00000081 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x00000082 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x00000083 TElement System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x00000084 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x00000085 System.Void System.Linq.Lookup`2_Grouping::.ctor()
// 0x00000086 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::.ctor(System.Int32)
// 0x00000087 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.IDisposable.Dispose()
// 0x00000088 System.Boolean System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::MoveNext()
// 0x00000089 TElement System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x0000008A System.Object System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.get_Current()
// 0x0000008B System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::.ctor(System.Int32)
// 0x0000008C System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.IDisposable.Dispose()
// 0x0000008D System.Boolean System.Linq.Lookup`2_<GetEnumerator>d__12::MoveNext()
// 0x0000008E System.Linq.IGrouping`2<TKey,TElement> System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.Generic.IEnumerator<System.Linq.IGrouping<TKey,TElement>>.get_Current()
// 0x0000008F System.Object System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.get_Current()
// 0x00000090 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x00000091 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x00000092 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x00000093 System.Void System.Linq.Set`1::Resize()
// 0x00000094 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x00000095 System.Void System.Linq.GroupedEnumerable`3::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000096 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.GroupedEnumerable`3::GetEnumerator()
// 0x00000097 System.Collections.IEnumerator System.Linq.GroupedEnumerable`3::System.Collections.IEnumerable.GetEnumerator()
// 0x00000098 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x00000099 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000009A System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000009B System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x0000009C System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x0000009D System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x0000009E System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x0000009F TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000A0 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000A1 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000A2 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000A3 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000A4 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000A5 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000A6 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000A7 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000A8 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000A9 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000AA System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x000000AB System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000AC TElement[] System.Linq.Buffer`1::ToArray()
static Il2CppMethodPointer s_methodPointers[172] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneElement_mD96D1249F5D42379E9417302B5F33DD99B51C863,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[172] = 
{
	0,
	0,
	4,
	4,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[60] = 
{
	{ 0x02000004, { 91, 4 } },
	{ 0x02000005, { 95, 9 } },
	{ 0x02000006, { 106, 7 } },
	{ 0x02000007, { 115, 10 } },
	{ 0x02000008, { 127, 11 } },
	{ 0x02000009, { 141, 9 } },
	{ 0x0200000A, { 153, 12 } },
	{ 0x0200000B, { 168, 1 } },
	{ 0x0200000C, { 169, 2 } },
	{ 0x0200000D, { 171, 12 } },
	{ 0x0200000E, { 183, 12 } },
	{ 0x0200000F, { 195, 6 } },
	{ 0x02000010, { 201, 2 } },
	{ 0x02000011, { 203, 4 } },
	{ 0x02000012, { 207, 3 } },
	{ 0x02000015, { 210, 17 } },
	{ 0x02000016, { 231, 5 } },
	{ 0x02000017, { 236, 1 } },
	{ 0x02000019, { 237, 8 } },
	{ 0x0200001B, { 245, 4 } },
	{ 0x0200001C, { 249, 3 } },
	{ 0x0200001D, { 252, 5 } },
	{ 0x0200001E, { 257, 7 } },
	{ 0x0200001F, { 264, 3 } },
	{ 0x02000020, { 267, 7 } },
	{ 0x02000021, { 274, 4 } },
	{ 0x06000007, { 0, 10 } },
	{ 0x06000008, { 10, 10 } },
	{ 0x06000009, { 20, 5 } },
	{ 0x0600000A, { 25, 5 } },
	{ 0x0600000B, { 30, 1 } },
	{ 0x0600000C, { 31, 2 } },
	{ 0x0600000D, { 33, 2 } },
	{ 0x0600000E, { 35, 4 } },
	{ 0x0600000F, { 39, 1 } },
	{ 0x06000010, { 40, 2 } },
	{ 0x06000011, { 42, 3 } },
	{ 0x06000012, { 45, 2 } },
	{ 0x06000013, { 47, 1 } },
	{ 0x06000014, { 48, 7 } },
	{ 0x06000015, { 55, 2 } },
	{ 0x06000016, { 57, 2 } },
	{ 0x06000017, { 59, 4 } },
	{ 0x06000018, { 63, 4 } },
	{ 0x06000019, { 67, 3 } },
	{ 0x0600001A, { 70, 4 } },
	{ 0x0600001B, { 74, 4 } },
	{ 0x0600001C, { 78, 3 } },
	{ 0x0600001D, { 81, 1 } },
	{ 0x0600001E, { 82, 1 } },
	{ 0x0600001F, { 83, 3 } },
	{ 0x06000020, { 86, 3 } },
	{ 0x06000021, { 89, 2 } },
	{ 0x06000030, { 104, 2 } },
	{ 0x06000035, { 113, 2 } },
	{ 0x0600003A, { 125, 2 } },
	{ 0x06000040, { 138, 3 } },
	{ 0x06000045, { 150, 3 } },
	{ 0x0600004A, { 165, 3 } },
	{ 0x0600006F, { 227, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[278] = 
{
	{ (Il2CppRGCTXDataType)2, 11961 },
	{ (Il2CppRGCTXDataType)3, 6124 },
	{ (Il2CppRGCTXDataType)2, 11962 },
	{ (Il2CppRGCTXDataType)2, 11963 },
	{ (Il2CppRGCTXDataType)3, 6125 },
	{ (Il2CppRGCTXDataType)2, 11964 },
	{ (Il2CppRGCTXDataType)2, 11965 },
	{ (Il2CppRGCTXDataType)3, 6126 },
	{ (Il2CppRGCTXDataType)2, 11966 },
	{ (Il2CppRGCTXDataType)3, 6127 },
	{ (Il2CppRGCTXDataType)2, 11967 },
	{ (Il2CppRGCTXDataType)3, 6128 },
	{ (Il2CppRGCTXDataType)2, 11968 },
	{ (Il2CppRGCTXDataType)2, 11969 },
	{ (Il2CppRGCTXDataType)3, 6129 },
	{ (Il2CppRGCTXDataType)2, 11970 },
	{ (Il2CppRGCTXDataType)2, 11971 },
	{ (Il2CppRGCTXDataType)3, 6130 },
	{ (Il2CppRGCTXDataType)2, 11972 },
	{ (Il2CppRGCTXDataType)3, 6131 },
	{ (Il2CppRGCTXDataType)2, 11973 },
	{ (Il2CppRGCTXDataType)3, 6132 },
	{ (Il2CppRGCTXDataType)3, 6133 },
	{ (Il2CppRGCTXDataType)2, 9495 },
	{ (Il2CppRGCTXDataType)3, 6134 },
	{ (Il2CppRGCTXDataType)2, 11974 },
	{ (Il2CppRGCTXDataType)3, 6135 },
	{ (Il2CppRGCTXDataType)3, 6136 },
	{ (Il2CppRGCTXDataType)2, 9502 },
	{ (Il2CppRGCTXDataType)3, 6137 },
	{ (Il2CppRGCTXDataType)3, 6138 },
	{ (Il2CppRGCTXDataType)2, 11975 },
	{ (Il2CppRGCTXDataType)3, 6139 },
	{ (Il2CppRGCTXDataType)2, 11976 },
	{ (Il2CppRGCTXDataType)3, 6140 },
	{ (Il2CppRGCTXDataType)3, 6141 },
	{ (Il2CppRGCTXDataType)2, 11977 },
	{ (Il2CppRGCTXDataType)2, 11978 },
	{ (Il2CppRGCTXDataType)3, 6142 },
	{ (Il2CppRGCTXDataType)3, 6143 },
	{ (Il2CppRGCTXDataType)2, 11979 },
	{ (Il2CppRGCTXDataType)3, 6144 },
	{ (Il2CppRGCTXDataType)2, 11980 },
	{ (Il2CppRGCTXDataType)3, 6145 },
	{ (Il2CppRGCTXDataType)3, 6146 },
	{ (Il2CppRGCTXDataType)2, 9534 },
	{ (Il2CppRGCTXDataType)3, 6147 },
	{ (Il2CppRGCTXDataType)3, 6148 },
	{ (Il2CppRGCTXDataType)2, 9549 },
	{ (Il2CppRGCTXDataType)3, 6149 },
	{ (Il2CppRGCTXDataType)2, 9542 },
	{ (Il2CppRGCTXDataType)2, 11981 },
	{ (Il2CppRGCTXDataType)3, 6150 },
	{ (Il2CppRGCTXDataType)3, 6151 },
	{ (Il2CppRGCTXDataType)3, 6152 },
	{ (Il2CppRGCTXDataType)2, 9550 },
	{ (Il2CppRGCTXDataType)3, 6153 },
	{ (Il2CppRGCTXDataType)2, 11982 },
	{ (Il2CppRGCTXDataType)3, 6154 },
	{ (Il2CppRGCTXDataType)2, 11983 },
	{ (Il2CppRGCTXDataType)2, 11984 },
	{ (Il2CppRGCTXDataType)2, 9554 },
	{ (Il2CppRGCTXDataType)2, 11985 },
	{ (Il2CppRGCTXDataType)2, 11986 },
	{ (Il2CppRGCTXDataType)2, 11987 },
	{ (Il2CppRGCTXDataType)2, 9556 },
	{ (Il2CppRGCTXDataType)2, 11988 },
	{ (Il2CppRGCTXDataType)2, 9558 },
	{ (Il2CppRGCTXDataType)2, 11989 },
	{ (Il2CppRGCTXDataType)3, 6155 },
	{ (Il2CppRGCTXDataType)2, 11990 },
	{ (Il2CppRGCTXDataType)2, 11991 },
	{ (Il2CppRGCTXDataType)2, 9561 },
	{ (Il2CppRGCTXDataType)2, 11992 },
	{ (Il2CppRGCTXDataType)2, 11993 },
	{ (Il2CppRGCTXDataType)2, 11994 },
	{ (Il2CppRGCTXDataType)2, 9563 },
	{ (Il2CppRGCTXDataType)2, 11995 },
	{ (Il2CppRGCTXDataType)2, 9565 },
	{ (Il2CppRGCTXDataType)2, 11996 },
	{ (Il2CppRGCTXDataType)3, 6156 },
	{ (Il2CppRGCTXDataType)2, 11997 },
	{ (Il2CppRGCTXDataType)2, 9570 },
	{ (Il2CppRGCTXDataType)2, 9572 },
	{ (Il2CppRGCTXDataType)2, 11998 },
	{ (Il2CppRGCTXDataType)3, 6157 },
	{ (Il2CppRGCTXDataType)2, 9575 },
	{ (Il2CppRGCTXDataType)2, 11999 },
	{ (Il2CppRGCTXDataType)3, 6158 },
	{ (Il2CppRGCTXDataType)2, 12000 },
	{ (Il2CppRGCTXDataType)2, 9578 },
	{ (Il2CppRGCTXDataType)3, 6159 },
	{ (Il2CppRGCTXDataType)3, 6160 },
	{ (Il2CppRGCTXDataType)2, 9582 },
	{ (Il2CppRGCTXDataType)3, 6161 },
	{ (Il2CppRGCTXDataType)3, 6162 },
	{ (Il2CppRGCTXDataType)2, 9594 },
	{ (Il2CppRGCTXDataType)2, 12001 },
	{ (Il2CppRGCTXDataType)3, 6163 },
	{ (Il2CppRGCTXDataType)3, 6164 },
	{ (Il2CppRGCTXDataType)2, 9596 },
	{ (Il2CppRGCTXDataType)2, 11875 },
	{ (Il2CppRGCTXDataType)3, 6165 },
	{ (Il2CppRGCTXDataType)3, 6166 },
	{ (Il2CppRGCTXDataType)2, 12002 },
	{ (Il2CppRGCTXDataType)3, 6167 },
	{ (Il2CppRGCTXDataType)3, 6168 },
	{ (Il2CppRGCTXDataType)2, 9606 },
	{ (Il2CppRGCTXDataType)2, 12003 },
	{ (Il2CppRGCTXDataType)3, 6169 },
	{ (Il2CppRGCTXDataType)3, 6170 },
	{ (Il2CppRGCTXDataType)3, 5684 },
	{ (Il2CppRGCTXDataType)3, 6171 },
	{ (Il2CppRGCTXDataType)2, 12004 },
	{ (Il2CppRGCTXDataType)3, 6172 },
	{ (Il2CppRGCTXDataType)3, 6173 },
	{ (Il2CppRGCTXDataType)2, 9618 },
	{ (Il2CppRGCTXDataType)2, 12005 },
	{ (Il2CppRGCTXDataType)3, 6174 },
	{ (Il2CppRGCTXDataType)3, 6175 },
	{ (Il2CppRGCTXDataType)3, 6176 },
	{ (Il2CppRGCTXDataType)3, 6177 },
	{ (Il2CppRGCTXDataType)3, 6178 },
	{ (Il2CppRGCTXDataType)3, 5689 },
	{ (Il2CppRGCTXDataType)3, 6179 },
	{ (Il2CppRGCTXDataType)2, 12006 },
	{ (Il2CppRGCTXDataType)3, 6180 },
	{ (Il2CppRGCTXDataType)3, 6181 },
	{ (Il2CppRGCTXDataType)2, 9631 },
	{ (Il2CppRGCTXDataType)2, 12007 },
	{ (Il2CppRGCTXDataType)3, 6182 },
	{ (Il2CppRGCTXDataType)3, 6183 },
	{ (Il2CppRGCTXDataType)2, 9633 },
	{ (Il2CppRGCTXDataType)2, 12008 },
	{ (Il2CppRGCTXDataType)3, 6184 },
	{ (Il2CppRGCTXDataType)3, 6185 },
	{ (Il2CppRGCTXDataType)2, 12009 },
	{ (Il2CppRGCTXDataType)3, 6186 },
	{ (Il2CppRGCTXDataType)3, 6187 },
	{ (Il2CppRGCTXDataType)2, 12010 },
	{ (Il2CppRGCTXDataType)3, 6188 },
	{ (Il2CppRGCTXDataType)3, 6189 },
	{ (Il2CppRGCTXDataType)2, 9648 },
	{ (Il2CppRGCTXDataType)2, 12011 },
	{ (Il2CppRGCTXDataType)3, 6190 },
	{ (Il2CppRGCTXDataType)3, 6191 },
	{ (Il2CppRGCTXDataType)3, 6192 },
	{ (Il2CppRGCTXDataType)3, 5698 },
	{ (Il2CppRGCTXDataType)2, 12012 },
	{ (Il2CppRGCTXDataType)3, 6193 },
	{ (Il2CppRGCTXDataType)3, 6194 },
	{ (Il2CppRGCTXDataType)2, 12013 },
	{ (Il2CppRGCTXDataType)3, 6195 },
	{ (Il2CppRGCTXDataType)3, 6196 },
	{ (Il2CppRGCTXDataType)2, 9664 },
	{ (Il2CppRGCTXDataType)2, 12014 },
	{ (Il2CppRGCTXDataType)3, 6197 },
	{ (Il2CppRGCTXDataType)3, 6198 },
	{ (Il2CppRGCTXDataType)3, 6199 },
	{ (Il2CppRGCTXDataType)3, 6200 },
	{ (Il2CppRGCTXDataType)3, 6201 },
	{ (Il2CppRGCTXDataType)3, 6202 },
	{ (Il2CppRGCTXDataType)3, 5703 },
	{ (Il2CppRGCTXDataType)2, 12015 },
	{ (Il2CppRGCTXDataType)3, 6203 },
	{ (Il2CppRGCTXDataType)3, 6204 },
	{ (Il2CppRGCTXDataType)2, 12016 },
	{ (Il2CppRGCTXDataType)3, 6205 },
	{ (Il2CppRGCTXDataType)3, 6206 },
	{ (Il2CppRGCTXDataType)3, 6207 },
	{ (Il2CppRGCTXDataType)3, 6208 },
	{ (Il2CppRGCTXDataType)3, 6209 },
	{ (Il2CppRGCTXDataType)3, 6210 },
	{ (Il2CppRGCTXDataType)2, 12017 },
	{ (Il2CppRGCTXDataType)2, 12018 },
	{ (Il2CppRGCTXDataType)3, 6211 },
	{ (Il2CppRGCTXDataType)2, 9699 },
	{ (Il2CppRGCTXDataType)2, 9693 },
	{ (Il2CppRGCTXDataType)3, 6212 },
	{ (Il2CppRGCTXDataType)2, 9692 },
	{ (Il2CppRGCTXDataType)2, 12019 },
	{ (Il2CppRGCTXDataType)3, 6213 },
	{ (Il2CppRGCTXDataType)3, 6214 },
	{ (Il2CppRGCTXDataType)3, 6215 },
	{ (Il2CppRGCTXDataType)3, 6216 },
	{ (Il2CppRGCTXDataType)2, 12020 },
	{ (Il2CppRGCTXDataType)3, 6217 },
	{ (Il2CppRGCTXDataType)2, 9715 },
	{ (Il2CppRGCTXDataType)2, 9707 },
	{ (Il2CppRGCTXDataType)3, 6218 },
	{ (Il2CppRGCTXDataType)3, 6219 },
	{ (Il2CppRGCTXDataType)2, 9706 },
	{ (Il2CppRGCTXDataType)2, 12021 },
	{ (Il2CppRGCTXDataType)3, 6220 },
	{ (Il2CppRGCTXDataType)3, 6221 },
	{ (Il2CppRGCTXDataType)3, 6222 },
	{ (Il2CppRGCTXDataType)2, 9719 },
	{ (Il2CppRGCTXDataType)3, 6223 },
	{ (Il2CppRGCTXDataType)2, 12022 },
	{ (Il2CppRGCTXDataType)3, 6224 },
	{ (Il2CppRGCTXDataType)3, 6225 },
	{ (Il2CppRGCTXDataType)2, 12023 },
	{ (Il2CppRGCTXDataType)2, 12024 },
	{ (Il2CppRGCTXDataType)2, 12025 },
	{ (Il2CppRGCTXDataType)3, 6226 },
	{ (Il2CppRGCTXDataType)2, 9731 },
	{ (Il2CppRGCTXDataType)3, 6227 },
	{ (Il2CppRGCTXDataType)2, 12026 },
	{ (Il2CppRGCTXDataType)3, 6228 },
	{ (Il2CppRGCTXDataType)2, 12026 },
	{ (Il2CppRGCTXDataType)2, 9755 },
	{ (Il2CppRGCTXDataType)3, 6229 },
	{ (Il2CppRGCTXDataType)3, 6230 },
	{ (Il2CppRGCTXDataType)3, 6231 },
	{ (Il2CppRGCTXDataType)3, 6232 },
	{ (Il2CppRGCTXDataType)2, 12027 },
	{ (Il2CppRGCTXDataType)2, 12028 },
	{ (Il2CppRGCTXDataType)2, 12029 },
	{ (Il2CppRGCTXDataType)3, 6233 },
	{ (Il2CppRGCTXDataType)3, 6234 },
	{ (Il2CppRGCTXDataType)2, 9751 },
	{ (Il2CppRGCTXDataType)2, 9754 },
	{ (Il2CppRGCTXDataType)3, 6235 },
	{ (Il2CppRGCTXDataType)3, 6236 },
	{ (Il2CppRGCTXDataType)2, 9758 },
	{ (Il2CppRGCTXDataType)3, 6237 },
	{ (Il2CppRGCTXDataType)2, 12030 },
	{ (Il2CppRGCTXDataType)2, 9748 },
	{ (Il2CppRGCTXDataType)2, 12031 },
	{ (Il2CppRGCTXDataType)3, 6238 },
	{ (Il2CppRGCTXDataType)3, 6239 },
	{ (Il2CppRGCTXDataType)3, 6240 },
	{ (Il2CppRGCTXDataType)2, 12032 },
	{ (Il2CppRGCTXDataType)3, 6241 },
	{ (Il2CppRGCTXDataType)3, 6242 },
	{ (Il2CppRGCTXDataType)3, 6243 },
	{ (Il2CppRGCTXDataType)2, 9773 },
	{ (Il2CppRGCTXDataType)3, 6244 },
	{ (Il2CppRGCTXDataType)2, 12033 },
	{ (Il2CppRGCTXDataType)2, 12034 },
	{ (Il2CppRGCTXDataType)3, 6245 },
	{ (Il2CppRGCTXDataType)3, 6246 },
	{ (Il2CppRGCTXDataType)2, 9794 },
	{ (Il2CppRGCTXDataType)3, 6247 },
	{ (Il2CppRGCTXDataType)2, 9795 },
	{ (Il2CppRGCTXDataType)3, 6248 },
	{ (Il2CppRGCTXDataType)2, 12035 },
	{ (Il2CppRGCTXDataType)3, 6249 },
	{ (Il2CppRGCTXDataType)3, 6250 },
	{ (Il2CppRGCTXDataType)2, 12036 },
	{ (Il2CppRGCTXDataType)3, 6251 },
	{ (Il2CppRGCTXDataType)3, 6252 },
	{ (Il2CppRGCTXDataType)2, 12037 },
	{ (Il2CppRGCTXDataType)3, 6253 },
	{ (Il2CppRGCTXDataType)3, 6254 },
	{ (Il2CppRGCTXDataType)3, 6255 },
	{ (Il2CppRGCTXDataType)2, 9826 },
	{ (Il2CppRGCTXDataType)3, 6256 },
	{ (Il2CppRGCTXDataType)2, 9835 },
	{ (Il2CppRGCTXDataType)3, 6257 },
	{ (Il2CppRGCTXDataType)2, 12038 },
	{ (Il2CppRGCTXDataType)2, 12039 },
	{ (Il2CppRGCTXDataType)3, 6258 },
	{ (Il2CppRGCTXDataType)3, 6259 },
	{ (Il2CppRGCTXDataType)3, 6260 },
	{ (Il2CppRGCTXDataType)3, 6261 },
	{ (Il2CppRGCTXDataType)3, 6262 },
	{ (Il2CppRGCTXDataType)3, 6263 },
	{ (Il2CppRGCTXDataType)2, 9851 },
	{ (Il2CppRGCTXDataType)2, 12040 },
	{ (Il2CppRGCTXDataType)3, 6264 },
	{ (Il2CppRGCTXDataType)3, 6265 },
	{ (Il2CppRGCTXDataType)2, 9855 },
	{ (Il2CppRGCTXDataType)3, 6266 },
	{ (Il2CppRGCTXDataType)2, 12041 },
	{ (Il2CppRGCTXDataType)2, 9865 },
	{ (Il2CppRGCTXDataType)2, 9863 },
	{ (Il2CppRGCTXDataType)2, 12042 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	172,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	60,
	s_rgctxIndices,
	278,
	s_rgctxValues,
	NULL,
};
