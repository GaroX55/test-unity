﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// <>f__AnonymousType0`1<System.Object>
struct U3CU3Ef__AnonymousType0_1_t20BC43518FD4AB8808CD6B771A612DEAE54B46A2;
// <>f__AnonymousType0`1<System.String>
struct U3CU3Ef__AnonymousType0_1_tF2713EB3F6BDBC9857E1D837120C63EADE37793E;
// <>f__AnonymousType1`4<System.Int32,System.Object,System.Object,System.Object>
struct U3CU3Ef__AnonymousType1_4_t8F6E190F196AB67919B6585AD93FC8CDE6FAA020;
// <>f__AnonymousType1`4<System.Int32,System.String,System.String,Newtonsoft.Json.Linq.JObject>
struct U3CU3Ef__AnonymousType1_4_t28B41C550A5824C1512F73D8BF89723305904D5B;
// <>f__AnonymousType1`4<System.Int32,System.String,System.String,System.Object>
struct U3CU3Ef__AnonymousType1_4_t18D80EC1767C732AF75BAC54DD26828E7C3DAFBA;
// MessageHandler
struct MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t0156D58B02F7F746339A104F9B83AD57B41079EB;
// Newtonsoft.Json.Linq.JObject
struct JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B;
// Newtonsoft.Json.Linq.JPropertyKeyedCollection
struct JPropertyKeyedCollection_t7AA777B96E974D521927B0E1384ACE9C720129C7;
// Newtonsoft.Json.Linq.JToken
struct JToken_tCCEF558996D47101E43F6436A874C249291581AA;
// Newtonsoft.Json.Linq.JTokenEqualityComparer
struct JTokenEqualityComparer_t31A66E45D57A2A631CDC7F61F9E2F0AD6B5E2B0B;
// Newtonsoft.Json.Linq.JTokenType[]
struct JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB;
// Rotate
struct Rotate_t763544D6383D5853BC2A73F921EB5C3A7A2F836C;
// Rotate/<>c
struct U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2/Entry<System.Int32,UnityMessage>[]
struct EntryU5BU5D_tD5E84D357C5D74CC87825265A77D2D862105828A;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,UnityMessage>
struct KeyCollection_tDB6F470C7C17C4C8CDA90A39D6F662D1A3B3C989;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,UnityMessage>
struct ValueCollection_t79EE40DB0B6FDE833898E8532DB8AA40DCF98A01;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t03608389BB57475AA3F4B2B79D176A27807BC884;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityMessage>
struct Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerable_1_t23242A32E926F36FD0286DE665AEF4B297BB7C59;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t7B82AA0F8B96BAAA21E36DDF7A1FE4348BDDBE95;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GlobalJavaObjectRef
struct GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityMessage
struct UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4;
// UnityMessageManager
struct UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F;
// UnityMessageManager/MessageDelegate
struct MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E;
// UnityMessageManager/MessageHandlerDelegate
struct MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ef__AnonymousType0_1_tF2713EB3F6BDBC9857E1D837120C63EADE37793E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ef__AnonymousType1_4_t18D80EC1767C732AF75BAC54DD26828E7C3DAFBA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ef__AnonymousType1_4_t28B41C550A5824C1512F73D8BF89723305904D5B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0C58A8D623E334F9AB068259F6E168424141F2C8;
IL2CPP_EXTERN_C String_t* _stringLiteral1AACCFB5B185786DFD1A9E053BAFDE0CA89BA47D;
IL2CPP_EXTERN_C String_t* _stringLiteral2B020927D3C6EB407223A1BAA3D6CE3597A3F88D;
IL2CPP_EXTERN_C String_t* _stringLiteral2B6A18940427F3D9DF878DBE7868694F57AFF364;
IL2CPP_EXTERN_C String_t* _stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C;
IL2CPP_EXTERN_C String_t* _stringLiteral74B39E69E7652005F14B9B3ADE70D6C8DE94C051;
IL2CPP_EXTERN_C String_t* _stringLiteral7A92F3D26362D6557D5701DE77A63A01DF61E57F;
IL2CPP_EXTERN_C String_t* _stringLiteral87EA5DFC8B8E384D848979496E706390B497E547;
IL2CPP_EXTERN_C String_t* _stringLiteral9057C537F34973CF261CD8B9624E74700BB9737A;
IL2CPP_EXTERN_C String_t* _stringLiteralA17C9AAA61E80A1BF71D0D850AF4E5BAA9800BBD;
IL2CPP_EXTERN_C String_t* _stringLiteralB540547305C0BAB0BDAB2AD954B540FF4E01E999;
IL2CPP_EXTERN_C String_t* _stringLiteralB93EC56608FB302FEA7A8019D7C0E8E7239FC033;
IL2CPP_EXTERN_C String_t* _stringLiteralC8CFFE09B180553EB553F7F5F9028062145F4AE0;
IL2CPP_EXTERN_C String_t* _stringLiteralD3ED06D8ADCBDFF6079A520D21BA08F90A850BB2;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralDF5E1AA3CE899D450ACCBFF1001276669BA2D6F1;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1_Invoke_mB86FC1B303E77C41ED0E94FC3592A9CF8DA571D5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_m93C9C887896A7CE63E5B6F2BE015A32C757FB41D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Remove_m4B784310D2BB991FB6BC9036625E476B2A4BF958_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_mE7EC42E3DBE2BF1335495A5EA5CB26B213784C41_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mC61E608C684061D4497B7700BF059B5DDB97CE4B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Extensions_Value_TisInt32_t585191389E07734F19F3156FF88FB3EF4800D102_mBBB1A858BA8163D3F40D727000420A5A3EE72290_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Extensions_Value_TisString_t_m5929A4D2823191FA198ACF62FB75D458648314E9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisUnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_mB6B969AE735C0C299C10DB75E705C75E6AFCEF47_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MessageHandler_getData_TisRuntimeObject_m7BDBAFA6583925FEFAA062BFF4909650CE5F28DF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MessageHandler_getData_TisString_t_mE3816A703418F72E8D5CCC065A81DBC611855D12_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Rotate_onMessage_m2EB175A859194F332EBA43D1F532207B84EA915C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3COnMouseDownU3Eb__4_0_m174DA0084BF158C76305355492BE3C1FDDAAB0B1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ef__AnonymousType0_1__ctor_m47192BF73012041255CE70DDB0C75BF2174D611E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ef__AnonymousType1_4__ctor_m253F43B5DCC70E3BE151417F11F720589BA93C3A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ef__AnonymousType1_4__ctor_mF727C14B93E2778E118B5C13FFCBEC6929BFCC03_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t MessageHandler_Deserialize_mA9693BA8D2C9FD955FEA9DD8E48D55E6584BB8DE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MessageHandler_send_m0BF4951456C3CA50EEF785A982A6F10D92BD0C5E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Rotate_Awake_m98272D8BD36400F05C40890A9F7E6AAA35AB7515_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Rotate_OnMouseDown_mA2F15CCF41D63C626D70DA7338B4146EC13B9988_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Rotate_Update_m7EECA0DFF07C6BCA773478F66A949EE038D28330_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Rotate_onDestroy_m36332C66DECAA5FF23044FB5D6873B7E38AA129A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Rotate_onMessage_m2EB175A859194F332EBA43D1F532207B84EA915C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec_U3COnMouseDownU3Eb__4_0_m174DA0084BF158C76305355492BE3C1FDDAAB0B1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__cctor_mDA82C2EF6063BB47B299296272C713873EB938C5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UnityMessageManager_SendMessageToRN_m7FA689191F69A7BD1CAF2218D61EE8BCE156B250_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UnityMessageManager_SendMessageToRN_m810379DB93ACA8FB4F37914CA49BA677BDE94E3E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UnityMessageManager__cctor_mDD0387729BD393CF289CB737BC70BF82CFAC885B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UnityMessageManager__ctor_mFB3651A2237E489ED8746D5B8FC5B5AB95FE7CE7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UnityMessageManager_add_OnMessage_m7B623FDD1EB187BC9593C933D5E01766FC8DB344_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UnityMessageManager_add_OnRNMessage_mD1D6F59672DB48353711FBD7CC5A1C5573FF33FA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UnityMessageManager_generateId_m39045BEBA86C4467AD5549DFA0DDCDF9A7A9CB89_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16AssemblyU2DCSharp_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UnityMessageManager_onRNMessage_m47BA244DB7A9E9A2486007E2F5217A03FFFC1AF8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UnityMessageManager_remove_OnMessage_mE747C20C6838E183F2C60960D2C697D7997588F4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UnityMessageManager_remove_OnRNMessage_m12B8512E35DC119F3800C8C8A05442F9C0A19B5E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UnityMessageManager_set_Instance_m25A5E4940172A7950BA2EED339C161A0C74B05AEAssemblyU2DCSharp_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UnityMessageManager_set_Instance_m25A5E4940172A7950BA2EED339C161A0C74B05AE_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};


// System.Object


// <>f__AnonymousType0`1<System.String>
struct  U3CU3Ef__AnonymousType0_1_tF2713EB3F6BDBC9857E1D837120C63EADE37793E  : public RuntimeObject
{
public:
	// <CallbackTest>j__TPar <>f__AnonymousType0`1::<CallbackTest>i__Field
	String_t* ___U3CCallbackTestU3Ei__Field_0;

public:
	inline static int32_t get_offset_of_U3CCallbackTestU3Ei__Field_0() { return static_cast<int32_t>(offsetof(U3CU3Ef__AnonymousType0_1_tF2713EB3F6BDBC9857E1D837120C63EADE37793E, ___U3CCallbackTestU3Ei__Field_0)); }
	inline String_t* get_U3CCallbackTestU3Ei__Field_0() const { return ___U3CCallbackTestU3Ei__Field_0; }
	inline String_t** get_address_of_U3CCallbackTestU3Ei__Field_0() { return &___U3CCallbackTestU3Ei__Field_0; }
	inline void set_U3CCallbackTestU3Ei__Field_0(String_t* value)
	{
		___U3CCallbackTestU3Ei__Field_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCallbackTestU3Ei__Field_0), (void*)value);
	}
};


// <>f__AnonymousType1`4<System.Int32,System.String,System.String,Newtonsoft.Json.Linq.JObject>
struct  U3CU3Ef__AnonymousType1_4_t28B41C550A5824C1512F73D8BF89723305904D5B  : public RuntimeObject
{
public:
	// <id>j__TPar <>f__AnonymousType1`4::<id>i__Field
	int32_t ___U3CidU3Ei__Field_0;
	// <seq>j__TPar <>f__AnonymousType1`4::<seq>i__Field
	String_t* ___U3CseqU3Ei__Field_1;
	// <name>j__TPar <>f__AnonymousType1`4::<name>i__Field
	String_t* ___U3CnameU3Ei__Field_2;
	// <data>j__TPar <>f__AnonymousType1`4::<data>i__Field
	JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * ___U3CdataU3Ei__Field_3;

public:
	inline static int32_t get_offset_of_U3CidU3Ei__Field_0() { return static_cast<int32_t>(offsetof(U3CU3Ef__AnonymousType1_4_t28B41C550A5824C1512F73D8BF89723305904D5B, ___U3CidU3Ei__Field_0)); }
	inline int32_t get_U3CidU3Ei__Field_0() const { return ___U3CidU3Ei__Field_0; }
	inline int32_t* get_address_of_U3CidU3Ei__Field_0() { return &___U3CidU3Ei__Field_0; }
	inline void set_U3CidU3Ei__Field_0(int32_t value)
	{
		___U3CidU3Ei__Field_0 = value;
	}

	inline static int32_t get_offset_of_U3CseqU3Ei__Field_1() { return static_cast<int32_t>(offsetof(U3CU3Ef__AnonymousType1_4_t28B41C550A5824C1512F73D8BF89723305904D5B, ___U3CseqU3Ei__Field_1)); }
	inline String_t* get_U3CseqU3Ei__Field_1() const { return ___U3CseqU3Ei__Field_1; }
	inline String_t** get_address_of_U3CseqU3Ei__Field_1() { return &___U3CseqU3Ei__Field_1; }
	inline void set_U3CseqU3Ei__Field_1(String_t* value)
	{
		___U3CseqU3Ei__Field_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CseqU3Ei__Field_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ei__Field_2() { return static_cast<int32_t>(offsetof(U3CU3Ef__AnonymousType1_4_t28B41C550A5824C1512F73D8BF89723305904D5B, ___U3CnameU3Ei__Field_2)); }
	inline String_t* get_U3CnameU3Ei__Field_2() const { return ___U3CnameU3Ei__Field_2; }
	inline String_t** get_address_of_U3CnameU3Ei__Field_2() { return &___U3CnameU3Ei__Field_2; }
	inline void set_U3CnameU3Ei__Field_2(String_t* value)
	{
		___U3CnameU3Ei__Field_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ei__Field_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdataU3Ei__Field_3() { return static_cast<int32_t>(offsetof(U3CU3Ef__AnonymousType1_4_t28B41C550A5824C1512F73D8BF89723305904D5B, ___U3CdataU3Ei__Field_3)); }
	inline JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * get_U3CdataU3Ei__Field_3() const { return ___U3CdataU3Ei__Field_3; }
	inline JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B ** get_address_of_U3CdataU3Ei__Field_3() { return &___U3CdataU3Ei__Field_3; }
	inline void set_U3CdataU3Ei__Field_3(JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * value)
	{
		___U3CdataU3Ei__Field_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdataU3Ei__Field_3), (void*)value);
	}
};


// <>f__AnonymousType1`4<System.Int32,System.String,System.String,System.Object>
struct  U3CU3Ef__AnonymousType1_4_t18D80EC1767C732AF75BAC54DD26828E7C3DAFBA  : public RuntimeObject
{
public:
	// <id>j__TPar <>f__AnonymousType1`4::<id>i__Field
	int32_t ___U3CidU3Ei__Field_0;
	// <seq>j__TPar <>f__AnonymousType1`4::<seq>i__Field
	String_t* ___U3CseqU3Ei__Field_1;
	// <name>j__TPar <>f__AnonymousType1`4::<name>i__Field
	String_t* ___U3CnameU3Ei__Field_2;
	// <data>j__TPar <>f__AnonymousType1`4::<data>i__Field
	RuntimeObject * ___U3CdataU3Ei__Field_3;

public:
	inline static int32_t get_offset_of_U3CidU3Ei__Field_0() { return static_cast<int32_t>(offsetof(U3CU3Ef__AnonymousType1_4_t18D80EC1767C732AF75BAC54DD26828E7C3DAFBA, ___U3CidU3Ei__Field_0)); }
	inline int32_t get_U3CidU3Ei__Field_0() const { return ___U3CidU3Ei__Field_0; }
	inline int32_t* get_address_of_U3CidU3Ei__Field_0() { return &___U3CidU3Ei__Field_0; }
	inline void set_U3CidU3Ei__Field_0(int32_t value)
	{
		___U3CidU3Ei__Field_0 = value;
	}

	inline static int32_t get_offset_of_U3CseqU3Ei__Field_1() { return static_cast<int32_t>(offsetof(U3CU3Ef__AnonymousType1_4_t18D80EC1767C732AF75BAC54DD26828E7C3DAFBA, ___U3CseqU3Ei__Field_1)); }
	inline String_t* get_U3CseqU3Ei__Field_1() const { return ___U3CseqU3Ei__Field_1; }
	inline String_t** get_address_of_U3CseqU3Ei__Field_1() { return &___U3CseqU3Ei__Field_1; }
	inline void set_U3CseqU3Ei__Field_1(String_t* value)
	{
		___U3CseqU3Ei__Field_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CseqU3Ei__Field_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ei__Field_2() { return static_cast<int32_t>(offsetof(U3CU3Ef__AnonymousType1_4_t18D80EC1767C732AF75BAC54DD26828E7C3DAFBA, ___U3CnameU3Ei__Field_2)); }
	inline String_t* get_U3CnameU3Ei__Field_2() const { return ___U3CnameU3Ei__Field_2; }
	inline String_t** get_address_of_U3CnameU3Ei__Field_2() { return &___U3CnameU3Ei__Field_2; }
	inline void set_U3CnameU3Ei__Field_2(String_t* value)
	{
		___U3CnameU3Ei__Field_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ei__Field_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdataU3Ei__Field_3() { return static_cast<int32_t>(offsetof(U3CU3Ef__AnonymousType1_4_t18D80EC1767C732AF75BAC54DD26828E7C3DAFBA, ___U3CdataU3Ei__Field_3)); }
	inline RuntimeObject * get_U3CdataU3Ei__Field_3() const { return ___U3CdataU3Ei__Field_3; }
	inline RuntimeObject ** get_address_of_U3CdataU3Ei__Field_3() { return &___U3CdataU3Ei__Field_3; }
	inline void set_U3CdataU3Ei__Field_3(RuntimeObject * value)
	{
		___U3CdataU3Ei__Field_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdataU3Ei__Field_3), (void*)value);
	}
};


// MessageHandler
struct  MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840  : public RuntimeObject
{
public:
	// System.Int32 MessageHandler::id
	int32_t ___id_0;
	// System.String MessageHandler::seq
	String_t* ___seq_1;
	// System.String MessageHandler::name
	String_t* ___name_2;
	// Newtonsoft.Json.Linq.JToken MessageHandler::data
	JToken_tCCEF558996D47101E43F6436A874C249291581AA * ___data_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_seq_1() { return static_cast<int32_t>(offsetof(MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840, ___seq_1)); }
	inline String_t* get_seq_1() const { return ___seq_1; }
	inline String_t** get_address_of_seq_1() { return &___seq_1; }
	inline void set_seq_1(String_t* value)
	{
		___seq_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___seq_1), (void*)value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_2), (void*)value);
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840, ___data_3)); }
	inline JToken_tCCEF558996D47101E43F6436A874C249291581AA * get_data_3() const { return ___data_3; }
	inline JToken_tCCEF558996D47101E43F6436A874C249291581AA ** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(JToken_tCCEF558996D47101E43F6436A874C249291581AA * value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_3), (void*)value);
	}
};


// Newtonsoft.Json.Linq.JToken
struct  JToken_tCCEF558996D47101E43F6436A874C249291581AA  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JToken::_parent
	JContainer_t0156D58B02F7F746339A104F9B83AD57B41079EB * ____parent_1;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::_previous
	JToken_tCCEF558996D47101E43F6436A874C249291581AA * ____previous_2;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::_next
	JToken_tCCEF558996D47101E43F6436A874C249291581AA * ____next_3;
	// System.Object Newtonsoft.Json.Linq.JToken::_annotations
	RuntimeObject * ____annotations_4;

public:
	inline static int32_t get_offset_of__parent_1() { return static_cast<int32_t>(offsetof(JToken_tCCEF558996D47101E43F6436A874C249291581AA, ____parent_1)); }
	inline JContainer_t0156D58B02F7F746339A104F9B83AD57B41079EB * get__parent_1() const { return ____parent_1; }
	inline JContainer_t0156D58B02F7F746339A104F9B83AD57B41079EB ** get_address_of__parent_1() { return &____parent_1; }
	inline void set__parent_1(JContainer_t0156D58B02F7F746339A104F9B83AD57B41079EB * value)
	{
		____parent_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____parent_1), (void*)value);
	}

	inline static int32_t get_offset_of__previous_2() { return static_cast<int32_t>(offsetof(JToken_tCCEF558996D47101E43F6436A874C249291581AA, ____previous_2)); }
	inline JToken_tCCEF558996D47101E43F6436A874C249291581AA * get__previous_2() const { return ____previous_2; }
	inline JToken_tCCEF558996D47101E43F6436A874C249291581AA ** get_address_of__previous_2() { return &____previous_2; }
	inline void set__previous_2(JToken_tCCEF558996D47101E43F6436A874C249291581AA * value)
	{
		____previous_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____previous_2), (void*)value);
	}

	inline static int32_t get_offset_of__next_3() { return static_cast<int32_t>(offsetof(JToken_tCCEF558996D47101E43F6436A874C249291581AA, ____next_3)); }
	inline JToken_tCCEF558996D47101E43F6436A874C249291581AA * get__next_3() const { return ____next_3; }
	inline JToken_tCCEF558996D47101E43F6436A874C249291581AA ** get_address_of__next_3() { return &____next_3; }
	inline void set__next_3(JToken_tCCEF558996D47101E43F6436A874C249291581AA * value)
	{
		____next_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____next_3), (void*)value);
	}

	inline static int32_t get_offset_of__annotations_4() { return static_cast<int32_t>(offsetof(JToken_tCCEF558996D47101E43F6436A874C249291581AA, ____annotations_4)); }
	inline RuntimeObject * get__annotations_4() const { return ____annotations_4; }
	inline RuntimeObject ** get_address_of__annotations_4() { return &____annotations_4; }
	inline void set__annotations_4(RuntimeObject * value)
	{
		____annotations_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____annotations_4), (void*)value);
	}
};

struct JToken_tCCEF558996D47101E43F6436A874C249291581AA_StaticFields
{
public:
	// Newtonsoft.Json.Linq.JTokenEqualityComparer Newtonsoft.Json.Linq.JToken::_equalityComparer
	JTokenEqualityComparer_t31A66E45D57A2A631CDC7F61F9E2F0AD6B5E2B0B * ____equalityComparer_0;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::BooleanTypes
	JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* ___BooleanTypes_5;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::NumberTypes
	JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* ___NumberTypes_6;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::StringTypes
	JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* ___StringTypes_7;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::GuidTypes
	JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* ___GuidTypes_8;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::TimeSpanTypes
	JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* ___TimeSpanTypes_9;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::UriTypes
	JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* ___UriTypes_10;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::CharTypes
	JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* ___CharTypes_11;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::DateTimeTypes
	JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* ___DateTimeTypes_12;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::BytesTypes
	JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* ___BytesTypes_13;

public:
	inline static int32_t get_offset_of__equalityComparer_0() { return static_cast<int32_t>(offsetof(JToken_tCCEF558996D47101E43F6436A874C249291581AA_StaticFields, ____equalityComparer_0)); }
	inline JTokenEqualityComparer_t31A66E45D57A2A631CDC7F61F9E2F0AD6B5E2B0B * get__equalityComparer_0() const { return ____equalityComparer_0; }
	inline JTokenEqualityComparer_t31A66E45D57A2A631CDC7F61F9E2F0AD6B5E2B0B ** get_address_of__equalityComparer_0() { return &____equalityComparer_0; }
	inline void set__equalityComparer_0(JTokenEqualityComparer_t31A66E45D57A2A631CDC7F61F9E2F0AD6B5E2B0B * value)
	{
		____equalityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____equalityComparer_0), (void*)value);
	}

	inline static int32_t get_offset_of_BooleanTypes_5() { return static_cast<int32_t>(offsetof(JToken_tCCEF558996D47101E43F6436A874C249291581AA_StaticFields, ___BooleanTypes_5)); }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* get_BooleanTypes_5() const { return ___BooleanTypes_5; }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB** get_address_of_BooleanTypes_5() { return &___BooleanTypes_5; }
	inline void set_BooleanTypes_5(JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* value)
	{
		___BooleanTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BooleanTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_NumberTypes_6() { return static_cast<int32_t>(offsetof(JToken_tCCEF558996D47101E43F6436A874C249291581AA_StaticFields, ___NumberTypes_6)); }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* get_NumberTypes_6() const { return ___NumberTypes_6; }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB** get_address_of_NumberTypes_6() { return &___NumberTypes_6; }
	inline void set_NumberTypes_6(JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* value)
	{
		___NumberTypes_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NumberTypes_6), (void*)value);
	}

	inline static int32_t get_offset_of_StringTypes_7() { return static_cast<int32_t>(offsetof(JToken_tCCEF558996D47101E43F6436A874C249291581AA_StaticFields, ___StringTypes_7)); }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* get_StringTypes_7() const { return ___StringTypes_7; }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB** get_address_of_StringTypes_7() { return &___StringTypes_7; }
	inline void set_StringTypes_7(JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* value)
	{
		___StringTypes_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StringTypes_7), (void*)value);
	}

	inline static int32_t get_offset_of_GuidTypes_8() { return static_cast<int32_t>(offsetof(JToken_tCCEF558996D47101E43F6436A874C249291581AA_StaticFields, ___GuidTypes_8)); }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* get_GuidTypes_8() const { return ___GuidTypes_8; }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB** get_address_of_GuidTypes_8() { return &___GuidTypes_8; }
	inline void set_GuidTypes_8(JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* value)
	{
		___GuidTypes_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GuidTypes_8), (void*)value);
	}

	inline static int32_t get_offset_of_TimeSpanTypes_9() { return static_cast<int32_t>(offsetof(JToken_tCCEF558996D47101E43F6436A874C249291581AA_StaticFields, ___TimeSpanTypes_9)); }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* get_TimeSpanTypes_9() const { return ___TimeSpanTypes_9; }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB** get_address_of_TimeSpanTypes_9() { return &___TimeSpanTypes_9; }
	inline void set_TimeSpanTypes_9(JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* value)
	{
		___TimeSpanTypes_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TimeSpanTypes_9), (void*)value);
	}

	inline static int32_t get_offset_of_UriTypes_10() { return static_cast<int32_t>(offsetof(JToken_tCCEF558996D47101E43F6436A874C249291581AA_StaticFields, ___UriTypes_10)); }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* get_UriTypes_10() const { return ___UriTypes_10; }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB** get_address_of_UriTypes_10() { return &___UriTypes_10; }
	inline void set_UriTypes_10(JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* value)
	{
		___UriTypes_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriTypes_10), (void*)value);
	}

	inline static int32_t get_offset_of_CharTypes_11() { return static_cast<int32_t>(offsetof(JToken_tCCEF558996D47101E43F6436A874C249291581AA_StaticFields, ___CharTypes_11)); }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* get_CharTypes_11() const { return ___CharTypes_11; }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB** get_address_of_CharTypes_11() { return &___CharTypes_11; }
	inline void set_CharTypes_11(JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* value)
	{
		___CharTypes_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CharTypes_11), (void*)value);
	}

	inline static int32_t get_offset_of_DateTimeTypes_12() { return static_cast<int32_t>(offsetof(JToken_tCCEF558996D47101E43F6436A874C249291581AA_StaticFields, ___DateTimeTypes_12)); }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* get_DateTimeTypes_12() const { return ___DateTimeTypes_12; }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB** get_address_of_DateTimeTypes_12() { return &___DateTimeTypes_12; }
	inline void set_DateTimeTypes_12(JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* value)
	{
		___DateTimeTypes_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DateTimeTypes_12), (void*)value);
	}

	inline static int32_t get_offset_of_BytesTypes_13() { return static_cast<int32_t>(offsetof(JToken_tCCEF558996D47101E43F6436A874C249291581AA_StaticFields, ___BytesTypes_13)); }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* get_BytesTypes_13() const { return ___BytesTypes_13; }
	inline JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB** get_address_of_BytesTypes_13() { return &___BytesTypes_13; }
	inline void set_BytesTypes_13(JTokenTypeU5BU5D_t3400E335922875276234E6A508943E0876FA7BBB* value)
	{
		___BytesTypes_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BytesTypes_13), (void*)value);
	}
};


// Rotate_<>c
struct  U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D_StaticFields
{
public:
	// Rotate_<>c Rotate_<>c::<>9
	U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D * ___U3CU3E9_0;
	// System.Action`1<System.Object> Rotate_<>c::<>9__4_0
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___U3CU3E9__4_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__4_0_1), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.Dictionary`2<System.Int32,UnityMessage>
struct  Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_tD5E84D357C5D74CC87825265A77D2D862105828A* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_tDB6F470C7C17C4C8CDA90A39D6F662D1A3B3C989 * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t79EE40DB0B6FDE833898E8532DB8AA40DCF98A01 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8, ___entries_1)); }
	inline EntryU5BU5D_tD5E84D357C5D74CC87825265A77D2D862105828A* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tD5E84D357C5D74CC87825265A77D2D862105828A** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tD5E84D357C5D74CC87825265A77D2D862105828A* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8, ___keys_7)); }
	inline KeyCollection_tDB6F470C7C17C4C8CDA90A39D6F662D1A3B3C989 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_tDB6F470C7C17C4C8CDA90A39D6F662D1A3B3C989 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_tDB6F470C7C17C4C8CDA90A39D6F662D1A3B3C989 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8, ___values_8)); }
	inline ValueCollection_t79EE40DB0B6FDE833898E8532DB8AA40DCF98A01 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t79EE40DB0B6FDE833898E8532DB8AA40DCF98A01 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t79EE40DB0B6FDE833898E8532DB8AA40DCF98A01 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.AndroidJavaObject
struct  AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D  : public RuntimeObject
{
public:
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaObject::m_jobject
	GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * ___m_jobject_1;
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaObject::m_jclass
	GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * ___m_jclass_2;

public:
	inline static int32_t get_offset_of_m_jobject_1() { return static_cast<int32_t>(offsetof(AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D, ___m_jobject_1)); }
	inline GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * get_m_jobject_1() const { return ___m_jobject_1; }
	inline GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 ** get_address_of_m_jobject_1() { return &___m_jobject_1; }
	inline void set_m_jobject_1(GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * value)
	{
		___m_jobject_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_jobject_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_jclass_2() { return static_cast<int32_t>(offsetof(AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D, ___m_jclass_2)); }
	inline GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * get_m_jclass_2() const { return ___m_jclass_2; }
	inline GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 ** get_address_of_m_jclass_2() { return &___m_jclass_2; }
	inline void set_m_jclass_2(GlobalJavaObjectRef_t2B9FA8DBBC53F0C0E0B57ACDC0FA9967CFB22DD0 * value)
	{
		___m_jclass_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_jclass_2), (void*)value);
	}
};

struct AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_StaticFields
{
public:
	// System.Boolean UnityEngine.AndroidJavaObject::enableDebugPrints
	bool ___enableDebugPrints_0;

public:
	inline static int32_t get_offset_of_enableDebugPrints_0() { return static_cast<int32_t>(offsetof(AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D_StaticFields, ___enableDebugPrints_0)); }
	inline bool get_enableDebugPrints_0() const { return ___enableDebugPrints_0; }
	inline bool* get_address_of_enableDebugPrints_0() { return &___enableDebugPrints_0; }
	inline void set_enableDebugPrints_0(bool value)
	{
		___enableDebugPrints_0 = value;
	}
};


// UnityMessage
struct  UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4  : public RuntimeObject
{
public:
	// System.String UnityMessage::name
	String_t* ___name_0;
	// Newtonsoft.Json.Linq.JObject UnityMessage::data
	JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * ___data_1;
	// System.Action`1<System.Object> UnityMessage::callBack
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___callBack_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4, ___data_1)); }
	inline JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * get_data_1() const { return ___data_1; }
	inline JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_1), (void*)value);
	}

	inline static int32_t get_offset_of_callBack_2() { return static_cast<int32_t>(offsetof(UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4, ___callBack_2)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get_callBack_2() const { return ___callBack_2; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of_callBack_2() { return &___callBack_2; }
	inline void set_callBack_2(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		___callBack_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callBack_2), (void*)value);
	}
};


// Newtonsoft.Json.Linq.JContainer
struct  JContainer_t0156D58B02F7F746339A104F9B83AD57B41079EB  : public JToken_tCCEF558996D47101E43F6436A874C249291581AA
{
public:
	// System.Object Newtonsoft.Json.Linq.JContainer::_syncRoot
	RuntimeObject * ____syncRoot_14;
	// System.Boolean Newtonsoft.Json.Linq.JContainer::_busy
	bool ____busy_15;

public:
	inline static int32_t get_offset_of__syncRoot_14() { return static_cast<int32_t>(offsetof(JContainer_t0156D58B02F7F746339A104F9B83AD57B41079EB, ____syncRoot_14)); }
	inline RuntimeObject * get__syncRoot_14() const { return ____syncRoot_14; }
	inline RuntimeObject ** get_address_of__syncRoot_14() { return &____syncRoot_14; }
	inline void set__syncRoot_14(RuntimeObject * value)
	{
		____syncRoot_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_14), (void*)value);
	}

	inline static int32_t get_offset_of__busy_15() { return static_cast<int32_t>(offsetof(JContainer_t0156D58B02F7F746339A104F9B83AD57B41079EB, ____busy_15)); }
	inline bool get__busy_15() const { return ____busy_15; }
	inline bool* get_address_of__busy_15() { return &____busy_15; }
	inline void set__busy_15(bool value)
	{
		____busy_15 = value;
	}
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.AndroidJavaClass
struct  AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE  : public AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D
{
public:

public:
};


// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};


// Newtonsoft.Json.Linq.JObject
struct  JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B  : public JContainer_t0156D58B02F7F746339A104F9B83AD57B41079EB
{
public:
	// Newtonsoft.Json.Linq.JPropertyKeyedCollection Newtonsoft.Json.Linq.JObject::_properties
	JPropertyKeyedCollection_t7AA777B96E974D521927B0E1384ACE9C720129C7 * ____properties_16;
	// System.ComponentModel.PropertyChangedEventHandler Newtonsoft.Json.Linq.JObject::PropertyChanged
	PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * ___PropertyChanged_17;

public:
	inline static int32_t get_offset_of__properties_16() { return static_cast<int32_t>(offsetof(JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B, ____properties_16)); }
	inline JPropertyKeyedCollection_t7AA777B96E974D521927B0E1384ACE9C720129C7 * get__properties_16() const { return ____properties_16; }
	inline JPropertyKeyedCollection_t7AA777B96E974D521927B0E1384ACE9C720129C7 ** get_address_of__properties_16() { return &____properties_16; }
	inline void set__properties_16(JPropertyKeyedCollection_t7AA777B96E974D521927B0E1384ACE9C720129C7 * value)
	{
		____properties_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____properties_16), (void*)value);
	}

	inline static int32_t get_offset_of_PropertyChanged_17() { return static_cast<int32_t>(offsetof(JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B, ___PropertyChanged_17)); }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * get_PropertyChanged_17() const { return ___PropertyChanged_17; }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 ** get_address_of_PropertyChanged_17() { return &___PropertyChanged_17; }
	inline void set_PropertyChanged_17(PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * value)
	{
		___PropertyChanged_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PropertyChanged_17), (void*)value);
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// System.Action`1<System.Object>
struct  Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0  : public MulticastDelegate_t
{
public:

public:
};


// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityMessageManager_MessageDelegate
struct  MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E  : public MulticastDelegate_t
{
public:

public:
};


// UnityMessageManager_MessageHandlerDelegate
struct  MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// Rotate
struct  Rotate_t763544D6383D5853BC2A73F921EB5C3A7A2F836C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Rotate::canRotate
	bool ___canRotate_4;

public:
	inline static int32_t get_offset_of_canRotate_4() { return static_cast<int32_t>(offsetof(Rotate_t763544D6383D5853BC2A73F921EB5C3A7A2F836C, ___canRotate_4)); }
	inline bool get_canRotate_4() const { return ___canRotate_4; }
	inline bool* get_address_of_canRotate_4() { return &___canRotate_4; }
	inline void set_canRotate_4(bool value)
	{
		___canRotate_4 = value;
	}
};


// UnityMessageManager
struct  UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityMessageManager_MessageDelegate UnityMessageManager::OnMessage
	MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * ___OnMessage_7;
	// UnityMessageManager_MessageHandlerDelegate UnityMessageManager::OnRNMessage
	MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * ___OnRNMessage_8;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityMessage> UnityMessageManager::waitCallbackMessageMap
	Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 * ___waitCallbackMessageMap_9;

public:
	inline static int32_t get_offset_of_OnMessage_7() { return static_cast<int32_t>(offsetof(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F, ___OnMessage_7)); }
	inline MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * get_OnMessage_7() const { return ___OnMessage_7; }
	inline MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E ** get_address_of_OnMessage_7() { return &___OnMessage_7; }
	inline void set_OnMessage_7(MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * value)
	{
		___OnMessage_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnMessage_7), (void*)value);
	}

	inline static int32_t get_offset_of_OnRNMessage_8() { return static_cast<int32_t>(offsetof(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F, ___OnRNMessage_8)); }
	inline MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * get_OnRNMessage_8() const { return ___OnRNMessage_8; }
	inline MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 ** get_address_of_OnRNMessage_8() { return &___OnRNMessage_8; }
	inline void set_OnRNMessage_8(MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * value)
	{
		___OnRNMessage_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnRNMessage_8), (void*)value);
	}

	inline static int32_t get_offset_of_waitCallbackMessageMap_9() { return static_cast<int32_t>(offsetof(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F, ___waitCallbackMessageMap_9)); }
	inline Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 * get_waitCallbackMessageMap_9() const { return ___waitCallbackMessageMap_9; }
	inline Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 ** get_address_of_waitCallbackMessageMap_9() { return &___waitCallbackMessageMap_9; }
	inline void set_waitCallbackMessageMap_9(Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 * value)
	{
		___waitCallbackMessageMap_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___waitCallbackMessageMap_9), (void*)value);
	}
};

struct UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_StaticFields
{
public:
	// System.Int32 UnityMessageManager::ID
	int32_t ___ID_5;
	// UnityMessageManager UnityMessageManager::<Instance>k__BackingField
	UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_ID_5() { return static_cast<int32_t>(offsetof(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_StaticFields, ___ID_5)); }
	inline int32_t get_ID_5() const { return ___ID_5; }
	inline int32_t* get_address_of_ID_5() { return &___ID_5; }
	inline void set_ID_5(int32_t value)
	{
		___ID_5 = value;
	}

	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInstanceU3Ek__BackingField_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 Newtonsoft.Json.Linq.Extensions::Value<System.Int32>(System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Extensions_Value_TisInt32_t585191389E07734F19F3156FF88FB3EF4800D102_mBBB1A858BA8163D3F40D727000420A5A3EE72290_gshared (RuntimeObject* ___value0, const RuntimeMethod* method);
// !!0 Newtonsoft.Json.Linq.Extensions::Value<System.Object>(System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Extensions_Value_TisRuntimeObject_m13B882A882008B8C6F5215EB5D958CF035224600_gshared (RuntimeObject* ___value0, const RuntimeMethod* method);
// System.Void <>f__AnonymousType1`4<System.Int32,System.Object,System.Object,System.Object>::.ctor(<id>j__TPar,<seq>j__TPar,<name>j__TPar,<data>j__TPar)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ef__AnonymousType1_4__ctor_m586B518A73603F8A551E4F89FD6E6A759CEC3B3B_gshared (U3CU3Ef__AnonymousType1_4_t8F6E190F196AB67919B6585AD93FC8CDE6FAA020 * __this, int32_t ___id0, RuntimeObject * ___seq1, RuntimeObject * ___name2, RuntimeObject * ___data3, const RuntimeMethod* method);
// T MessageHandler::getData<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * MessageHandler_getData_TisRuntimeObject_m7BDBAFA6583925FEFAA062BFF4909650CE5F28DF_gshared (MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * __this, const RuntimeMethod* method);
// System.Void <>f__AnonymousType0`1<System.Object>::.ctor(<CallbackTest>j__TPar)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ef__AnonymousType0_1__ctor_mB2A7DADA705E8B05DF2E87663EA504CD3438AC26_gshared (U3CU3Ef__AnonymousType0_1_t20BC43518FD4AB8808CD6B771A612DEAE54B46A2 * __this, RuntimeObject * ___CallbackTest0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m80EDFEAC4927F588A7A702F81524EDBFA8603FE2_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_mF7AEA0EFA07EEBC1A4B283A26A7CB720EE7A4C20_gshared (Dictionary_2_t03608389BB57475AA3F4B2B79D176A27807BC884 * __this, int32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(!0,!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_m867F6DA953678D0333A55270B7C6EF38EFC293FF_gshared (Dictionary_2_t03608389BB57475AA3F4B2B79D176A27807BC884 * __this, int32_t ___key0, RuntimeObject ** ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_Remove_m2204D6D532702FD13AB2A9AD8DB538E4E8FB1913_gshared (Dictionary_2_t03608389BB57475AA3F4B2B79D176A27807BC884 * __this, int32_t ___key0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::Invoke(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1_Invoke_mB86FC1B303E77C41ED0E94FC3592A9CF8DA571D5_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m7D745ADE56151C2895459668F4A4242985E526D8_gshared (Dictionary_2_t03608389BB57475AA3F4B2B79D176A27807BC884 * __this, const RuntimeMethod* method);

// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * JObject_Parse_mB74456E84D28ED826713880458EEA3EBE06CD518 (String_t* ___json0, const RuntimeMethod* method);
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JObject::GetValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR JToken_tCCEF558996D47101E43F6436A874C249291581AA * JObject_GetValue_m5A58D85CE54809916F6F0EFC97F731A4A7BFC01F (JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * __this, String_t* ___propertyName0, const RuntimeMethod* method);
// !!0 Newtonsoft.Json.Linq.Extensions::Value<System.Int32>(System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>)
inline int32_t Extensions_Value_TisInt32_t585191389E07734F19F3156FF88FB3EF4800D102_mBBB1A858BA8163D3F40D727000420A5A3EE72290 (RuntimeObject* ___value0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (RuntimeObject*, const RuntimeMethod*))Extensions_Value_TisInt32_t585191389E07734F19F3156FF88FB3EF4800D102_mBBB1A858BA8163D3F40D727000420A5A3EE72290_gshared)(___value0, method);
}
// !!0 Newtonsoft.Json.Linq.Extensions::Value<System.String>(System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>)
inline String_t* Extensions_Value_TisString_t_m5929A4D2823191FA198ACF62FB75D458648314E9 (RuntimeObject* ___value0, const RuntimeMethod* method)
{
	return ((  String_t* (*) (RuntimeObject*, const RuntimeMethod*))Extensions_Value_TisRuntimeObject_m13B882A882008B8C6F5215EB5D958CF035224600_gshared)(___value0, method);
}
// System.Void MessageHandler::.ctor(System.Int32,System.String,System.String,Newtonsoft.Json.Linq.JToken)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageHandler__ctor_mD413687341FACD003432FE926B76CC1DE2E78EFC (MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * __this, int32_t ___id0, String_t* ___seq1, String_t* ___name2, JToken_tCCEF558996D47101E43F6436A874C249291581AA * ___data3, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void <>f__AnonymousType1`4<System.Int32,System.String,System.String,System.Object>::.ctor(<id>j__TPar,<seq>j__TPar,<name>j__TPar,<data>j__TPar)
inline void U3CU3Ef__AnonymousType1_4__ctor_mF727C14B93E2778E118B5C13FFCBEC6929BFCC03 (U3CU3Ef__AnonymousType1_4_t18D80EC1767C732AF75BAC54DD26828E7C3DAFBA * __this, int32_t ___id0, String_t* ___seq1, String_t* ___name2, RuntimeObject * ___data3, const RuntimeMethod* method)
{
	((  void (*) (U3CU3Ef__AnonymousType1_4_t18D80EC1767C732AF75BAC54DD26828E7C3DAFBA *, int32_t, String_t*, String_t*, RuntimeObject *, const RuntimeMethod*))U3CU3Ef__AnonymousType1_4__ctor_m586B518A73603F8A551E4F89FD6E6A759CEC3B3B_gshared)(__this, ___id0, ___seq1, ___name2, ___data3, method);
}
// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject::FromObject(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * JObject_FromObject_mF56A1A94F0D967B8EE58CD8A0C9BB7EDDDDD94E1 (RuntimeObject * ___o0, const RuntimeMethod* method);
// UnityMessageManager UnityMessageManager::get_Instance()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16_inline (const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void UnityMessageManager::SendMessageToRN(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_SendMessageToRN_m7FA689191F69A7BD1CAF2218D61EE8BCE156B250 (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void UnityMessageManager/MessageHandlerDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageHandlerDelegate__ctor_m58386047CB442F144728F0A568B26FDDDB0EE6EA (MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityMessageManager::add_OnRNMessage(UnityMessageManager/MessageHandlerDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_add_OnRNMessage_mD1D6F59672DB48353711FBD7CC5A1C5573FF33FA (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * __this, MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * ___value0, const RuntimeMethod* method);
// System.Void UnityMessageManager::remove_OnRNMessage(UnityMessageManager/MessageHandlerDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_remove_OnRNMessage_m12B8512E35DC119F3800C8C8A05442F9C0A19B5E (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * __this, MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * ___value0, const RuntimeMethod* method);
// T MessageHandler::getData<System.String>()
inline String_t* MessageHandler_getData_TisString_t_mE3816A703418F72E8D5CCC065A81DBC611855D12 (MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 *, const RuntimeMethod*))MessageHandler_getData_TisRuntimeObject_m7BDBAFA6583925FEFAA062BFF4909650CE5F28DF_gshared)(__this, method);
}
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void <>f__AnonymousType0`1<System.String>::.ctor(<CallbackTest>j__TPar)
inline void U3CU3Ef__AnonymousType0_1__ctor_m47192BF73012041255CE70DDB0C75BF2174D611E (U3CU3Ef__AnonymousType0_1_tF2713EB3F6BDBC9857E1D837120C63EADE37793E * __this, String_t* ___CallbackTest0, const RuntimeMethod* method)
{
	((  void (*) (U3CU3Ef__AnonymousType0_1_tF2713EB3F6BDBC9857E1D837120C63EADE37793E *, String_t*, const RuntimeMethod*))U3CU3Ef__AnonymousType0_1__ctor_mB2A7DADA705E8B05DF2E87663EA504CD3438AC26_gshared)(__this, ___CallbackTest0, method);
}
// System.Void MessageHandler::send(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageHandler_send_m0BF4951456C3CA50EEF785A982A6F10D92BD0C5E (MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * __this, RuntimeObject * ___data0, const RuntimeMethod* method);
// System.Void UnityMessage::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessage__ctor_m76E68BAF631BE07953E806CA26C480BF23B2ED45 (UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510 (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, ___object0, ___method1, method);
}
// System.Void UnityMessageManager::SendMessageToRN(UnityMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_SendMessageToRN_m810379DB93ACA8FB4F37914CA49BA677BDE94E3E (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * __this, UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * ___message0, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Transform_get_localRotation_mEDA319E1B42EF12A19A95AC0824345B6574863FE (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05 (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_op_Multiply_mDB9F738AA8160E3D85549F4FEDA23BC658B5A790 (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___lhs0, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rhs1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Void Rotate/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mBFB4BD35AF855B60B3202619E831D4FB394CFC48 (U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method);
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1 (Delegate_t * ___a0, Delegate_t * ___b1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D (Delegate_t * ___source0, Delegate_t * ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m4DC90770AD6084E4B1B8489C6B41205DC020C207 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___target0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<UnityMessageManager>()
inline UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * GameObject_AddComponent_TisUnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_mB6B969AE735C0C299C10DB75E705C75E6AFCEF47 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m80EDFEAC4927F588A7A702F81524EDBFA8603FE2_gshared)(__this, method);
}
// System.Void UnityMessageManager::set_Instance(UnityMessageManager)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void UnityMessageManager_set_Instance_m25A5E4940172A7950BA2EED339C161A0C74B05AE_inline (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * ___value0, const RuntimeMethod* method);
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672 (const RuntimeMethod* method);
// System.Void UnityEngine.AndroidJavaClass::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidJavaClass__ctor_mAE416E812DB3911279C0FE87A7760247CE1BBFA8 (AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * __this, String_t* ___className0, const RuntimeMethod* method);
// System.Void UnityEngine.AndroidJavaObject::CallStatic(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidJavaObject_CallStatic_m12D78F5584C63F5D8B2344CBA8611EFBDDA669AF (AndroidJavaObject_t31F4DD4D4523A77B8AF16FE422B7426248E3093D * __this, String_t* ___methodName0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method);
// System.Void UnityMessageManager::onUnityMessage(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_onUnityMessage_mC3031C4585567E720F536DB4EE28B0C20AB8A7C4 (String_t* ___message0, const RuntimeMethod* method);
// System.Int32 UnityMessageManager::generateId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityMessageManager_generateId_m39045BEBA86C4467AD5549DFA0DDCDF9A7A9CB89 (const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityMessage>::Add(!0,!1)
inline void Dictionary_2_Add_m93C9C887896A7CE63E5B6F2BE015A32C757FB41D (Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 * __this, int32_t ___key0, UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 *, int32_t, UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 *, const RuntimeMethod*))Dictionary_2_Add_mF7AEA0EFA07EEBC1A4B283A26A7CB720EE7A4C20_gshared)(__this, ___key0, ___value1, method);
}
// System.Void <>f__AnonymousType1`4<System.Int32,System.String,System.String,Newtonsoft.Json.Linq.JObject>::.ctor(<id>j__TPar,<seq>j__TPar,<name>j__TPar,<data>j__TPar)
inline void U3CU3Ef__AnonymousType1_4__ctor_m253F43B5DCC70E3BE151417F11F720589BA93C3A (U3CU3Ef__AnonymousType1_4_t28B41C550A5824C1512F73D8BF89723305904D5B * __this, int32_t ___id0, String_t* ___seq1, String_t* ___name2, JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * ___data3, const RuntimeMethod* method)
{
	((  void (*) (U3CU3Ef__AnonymousType1_4_t28B41C550A5824C1512F73D8BF89723305904D5B *, int32_t, String_t*, String_t*, JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B *, const RuntimeMethod*))U3CU3Ef__AnonymousType1_4__ctor_m586B518A73603F8A551E4F89FD6E6A759CEC3B3B_gshared)(__this, ___id0, ___seq1, ___name2, ___data3, method);
}
// System.Void UnityMessageManager/MessageDelegate::Invoke(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageDelegate_Invoke_m2D199E1633D3EFBEA727164B8A3F7888E6A4F801 (MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Boolean System.String::StartsWith(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_StartsWith_m7D468FB7C801D9C2DBEEEEC86F8BA8F4EC3243C1 (String_t* __this, String_t* ___value0, const RuntimeMethod* method);
// System.String System.String::Replace(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Replace_m970DFB0A280952FA7D3BA20AB7A8FB9F80CF6470 (String_t* __this, String_t* ___oldValue0, String_t* ___newValue1, const RuntimeMethod* method);
// MessageHandler MessageHandler::Deserialize(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * MessageHandler_Deserialize_mA9693BA8D2C9FD955FEA9DD8E48D55E6584BB8DE (String_t* ___message0, const RuntimeMethod* method);
// System.Boolean System.String::Equals(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Equals_m9C4D78DFA0979504FE31429B64A4C26DF48020D1 (String_t* __this, String_t* ___value0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityMessage>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_mE7EC42E3DBE2BF1335495A5EA5CB26B213784C41 (Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 * __this, int32_t ___key0, UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 ** ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 *, int32_t, UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m867F6DA953678D0333A55270B7C6EF38EFC293FF_gshared)(__this, ___key0, ___value1, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityMessage>::Remove(!0)
inline bool Dictionary_2_Remove_m4B784310D2BB991FB6BC9036625E476B2A4BF958 (Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 * __this, int32_t ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 *, int32_t, const RuntimeMethod*))Dictionary_2_Remove_m2204D6D532702FD13AB2A9AD8DB538E4E8FB1913_gshared)(__this, ___key0, method);
}
// T MessageHandler::getData<System.Object>()
inline RuntimeObject * MessageHandler_getData_TisRuntimeObject_m7BDBAFA6583925FEFAA062BFF4909650CE5F28DF (MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject * (*) (MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 *, const RuntimeMethod*))MessageHandler_getData_TisRuntimeObject_m7BDBAFA6583925FEFAA062BFF4909650CE5F28DF_gshared)(__this, method);
}
// System.Void System.Action`1<System.Object>::Invoke(!0)
inline void Action_1_Invoke_mB86FC1B303E77C41ED0E94FC3592A9CF8DA571D5 (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *, RuntimeObject *, const RuntimeMethod*))Action_1_Invoke_mB86FC1B303E77C41ED0E94FC3592A9CF8DA571D5_gshared)(__this, ___obj0, method);
}
// System.Void UnityMessageManager/MessageHandlerDelegate::Invoke(MessageHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageHandlerDelegate_Invoke_mF982647CDFA5782B9ABAF328FF4070492A364B43 (MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * __this, MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * ___handler0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityMessage>::.ctor()
inline void Dictionary_2__ctor_mC61E608C684061D4497B7700BF059B5DDB97CE4B (Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 *, const RuntimeMethod*))Dictionary_2__ctor_m7D745ADE56151C2895459668F4A4242985E526D8_gshared)(__this, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// MessageHandler MessageHandler::Deserialize(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * MessageHandler_Deserialize_mA9693BA8D2C9FD955FEA9DD8E48D55E6584BB8DE (String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageHandler_Deserialize_mA9693BA8D2C9FD955FEA9DD8E48D55E6584BB8DE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * V_0 = NULL;
	{
		// JObject m = JObject.Parse(message);
		String_t* L_0 = ___message0;
		JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * L_1 = JObject_Parse_mB74456E84D28ED826713880458EEA3EBE06CD518(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// MessageHandler handler = new MessageHandler(
		//     m.GetValue("id").Value<int>(),
		//     m.GetValue("seq").Value<string>(),
		//     m.GetValue("name").Value<string>(),
		//     m.GetValue("data")
		// );
		JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * L_2 = V_0;
		NullCheck(L_2);
		JToken_tCCEF558996D47101E43F6436A874C249291581AA * L_3 = JObject_GetValue_m5A58D85CE54809916F6F0EFC97F731A4A7BFC01F(L_2, _stringLiteral87EA5DFC8B8E384D848979496E706390B497E547, /*hidden argument*/NULL);
		int32_t L_4 = Extensions_Value_TisInt32_t585191389E07734F19F3156FF88FB3EF4800D102_mBBB1A858BA8163D3F40D727000420A5A3EE72290(L_3, /*hidden argument*/Extensions_Value_TisInt32_t585191389E07734F19F3156FF88FB3EF4800D102_mBBB1A858BA8163D3F40D727000420A5A3EE72290_RuntimeMethod_var);
		JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * L_5 = V_0;
		NullCheck(L_5);
		JToken_tCCEF558996D47101E43F6436A874C249291581AA * L_6 = JObject_GetValue_m5A58D85CE54809916F6F0EFC97F731A4A7BFC01F(L_5, _stringLiteral1AACCFB5B185786DFD1A9E053BAFDE0CA89BA47D, /*hidden argument*/NULL);
		String_t* L_7 = Extensions_Value_TisString_t_m5929A4D2823191FA198ACF62FB75D458648314E9(L_6, /*hidden argument*/Extensions_Value_TisString_t_m5929A4D2823191FA198ACF62FB75D458648314E9_RuntimeMethod_var);
		JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * L_8 = V_0;
		NullCheck(L_8);
		JToken_tCCEF558996D47101E43F6436A874C249291581AA * L_9 = JObject_GetValue_m5A58D85CE54809916F6F0EFC97F731A4A7BFC01F(L_8, _stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		String_t* L_10 = Extensions_Value_TisString_t_m5929A4D2823191FA198ACF62FB75D458648314E9(L_9, /*hidden argument*/Extensions_Value_TisString_t_m5929A4D2823191FA198ACF62FB75D458648314E9_RuntimeMethod_var);
		JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * L_11 = V_0;
		NullCheck(L_11);
		JToken_tCCEF558996D47101E43F6436A874C249291581AA * L_12 = JObject_GetValue_m5A58D85CE54809916F6F0EFC97F731A4A7BFC01F(L_11, _stringLiteralA17C9AAA61E80A1BF71D0D850AF4E5BAA9800BBD, /*hidden argument*/NULL);
		MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * L_13 = (MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 *)il2cpp_codegen_object_new(MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840_il2cpp_TypeInfo_var);
		MessageHandler__ctor_mD413687341FACD003432FE926B76CC1DE2E78EFC(L_13, L_4, L_7, L_10, L_12, /*hidden argument*/NULL);
		// return handler;
		return L_13;
	}
}
// System.Void MessageHandler::.ctor(System.Int32,System.String,System.String,Newtonsoft.Json.Linq.JToken)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageHandler__ctor_mD413687341FACD003432FE926B76CC1DE2E78EFC (MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * __this, int32_t ___id0, String_t* ___seq1, String_t* ___name2, JToken_tCCEF558996D47101E43F6436A874C249291581AA * ___data3, const RuntimeMethod* method)
{
	{
		// public MessageHandler(int id, string seq, string name, JToken data)
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// this.id = id;
		int32_t L_0 = ___id0;
		__this->set_id_0(L_0);
		// this.seq = seq;
		String_t* L_1 = ___seq1;
		__this->set_seq_1(L_1);
		// this.name = name;
		String_t* L_2 = ___name2;
		__this->set_name_2(L_2);
		// this.data = data;
		JToken_tCCEF558996D47101E43F6436A874C249291581AA * L_3 = ___data3;
		__this->set_data_3(L_3);
		// }
		return;
	}
}
// System.Void MessageHandler::send(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageHandler_send_m0BF4951456C3CA50EEF785A982A6F10D92BD0C5E (MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * __this, RuntimeObject * ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageHandler_send_m0BF4951456C3CA50EEF785A982A6F10D92BD0C5E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * V_0 = NULL;
	{
		// JObject o = JObject.FromObject(new
		// {
		//     id = id,
		//     seq = "end",
		//     name = name,
		//     data = data
		// });
		int32_t L_0 = __this->get_id_0();
		String_t* L_1 = __this->get_name_2();
		RuntimeObject * L_2 = ___data0;
		U3CU3Ef__AnonymousType1_4_t18D80EC1767C732AF75BAC54DD26828E7C3DAFBA * L_3 = (U3CU3Ef__AnonymousType1_4_t18D80EC1767C732AF75BAC54DD26828E7C3DAFBA *)il2cpp_codegen_object_new(U3CU3Ef__AnonymousType1_4_t18D80EC1767C732AF75BAC54DD26828E7C3DAFBA_il2cpp_TypeInfo_var);
		U3CU3Ef__AnonymousType1_4__ctor_mF727C14B93E2778E118B5C13FFCBEC6929BFCC03(L_3, L_0, _stringLiteral7A92F3D26362D6557D5701DE77A63A01DF61E57F, L_1, L_2, /*hidden argument*/U3CU3Ef__AnonymousType1_4__ctor_mF727C14B93E2778E118B5C13FFCBEC6929BFCC03_RuntimeMethod_var);
		JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * L_4 = JObject_FromObject_mF56A1A94F0D967B8EE58CD8A0C9BB7EDDDDD94E1(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// UnityMessageManager.Instance.SendMessageToRN(UnityMessageManager.MessagePrefix + o.ToString());
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * L_5 = UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16_inline(/*hidden argument*/NULL);
		JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		String_t* L_8 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral0C58A8D623E334F9AB068259F6E168424141F2C8, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		UnityMessageManager_SendMessageToRN_m7FA689191F69A7BD1CAF2218D61EE8BCE156B250(L_5, L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Rotate::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rotate_Awake_m98272D8BD36400F05C40890A9F7E6AAA35AB7515 (Rotate_t763544D6383D5853BC2A73F921EB5C3A7A2F836C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rotate_Awake_m98272D8BD36400F05C40890A9F7E6AAA35AB7515_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// UnityMessageManager.Instance.OnRNMessage += onMessage;
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * L_0 = UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16_inline(/*hidden argument*/NULL);
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_1 = (MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 *)il2cpp_codegen_object_new(MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101_il2cpp_TypeInfo_var);
		MessageHandlerDelegate__ctor_m58386047CB442F144728F0A568B26FDDDB0EE6EA(L_1, __this, (intptr_t)((intptr_t)Rotate_onMessage_m2EB175A859194F332EBA43D1F532207B84EA915C_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		UnityMessageManager_add_OnRNMessage_mD1D6F59672DB48353711FBD7CC5A1C5573FF33FA(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Rotate::onDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rotate_onDestroy_m36332C66DECAA5FF23044FB5D6873B7E38AA129A (Rotate_t763544D6383D5853BC2A73F921EB5C3A7A2F836C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rotate_onDestroy_m36332C66DECAA5FF23044FB5D6873B7E38AA129A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// UnityMessageManager.Instance.OnRNMessage -= onMessage;
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * L_0 = UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16_inline(/*hidden argument*/NULL);
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_1 = (MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 *)il2cpp_codegen_object_new(MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101_il2cpp_TypeInfo_var);
		MessageHandlerDelegate__ctor_m58386047CB442F144728F0A568B26FDDDB0EE6EA(L_1, __this, (intptr_t)((intptr_t)Rotate_onMessage_m2EB175A859194F332EBA43D1F532207B84EA915C_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		UnityMessageManager_remove_OnRNMessage_m12B8512E35DC119F3800C8C8A05442F9C0A19B5E(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Rotate::onMessage(MessageHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rotate_onMessage_m2EB175A859194F332EBA43D1F532207B84EA915C (Rotate_t763544D6383D5853BC2A73F921EB5C3A7A2F836C * __this, MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rotate_onMessage_m2EB175A859194F332EBA43D1F532207B84EA915C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// var data = message.getData<string>();
		MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * L_0 = ___message0;
		NullCheck(L_0);
		String_t* L_1 = MessageHandler_getData_TisString_t_mE3816A703418F72E8D5CCC065A81DBC611855D12(L_0, /*hidden argument*/MessageHandler_getData_TisString_t_mE3816A703418F72E8D5CCC065A81DBC611855D12_RuntimeMethod_var);
		V_0 = L_1;
		// Debug.Log("onMessage:" + data);
		String_t* L_2 = V_0;
		String_t* L_3 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral2B6A18940427F3D9DF878DBE7868694F57AFF364, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_3, /*hidden argument*/NULL);
		// canRotate = !canRotate;
		bool L_4 = __this->get_canRotate_4();
		__this->set_canRotate_4((bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0));
		// message.send(new { CallbackTest = "I am Unity callback" });
		MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * L_5 = ___message0;
		U3CU3Ef__AnonymousType0_1_tF2713EB3F6BDBC9857E1D837120C63EADE37793E * L_6 = (U3CU3Ef__AnonymousType0_1_tF2713EB3F6BDBC9857E1D837120C63EADE37793E *)il2cpp_codegen_object_new(U3CU3Ef__AnonymousType0_1_tF2713EB3F6BDBC9857E1D837120C63EADE37793E_il2cpp_TypeInfo_var);
		U3CU3Ef__AnonymousType0_1__ctor_m47192BF73012041255CE70DDB0C75BF2174D611E(L_6, _stringLiteralC8CFFE09B180553EB553F7F5F9028062145F4AE0, /*hidden argument*/U3CU3Ef__AnonymousType0_1__ctor_m47192BF73012041255CE70DDB0C75BF2174D611E_RuntimeMethod_var);
		NullCheck(L_5);
		MessageHandler_send_m0BF4951456C3CA50EEF785A982A6F10D92BD0C5E(L_5, L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Rotate::OnMouseDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rotate_OnMouseDown_mA2F15CCF41D63C626D70DA7338B4146EC13B9988 (Rotate_t763544D6383D5853BC2A73F921EB5C3A7A2F836C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rotate_OnMouseDown_mA2F15CCF41D63C626D70DA7338B4146EC13B9988_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * G_B2_0 = NULL;
	UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * G_B2_1 = NULL;
	UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * G_B2_2 = NULL;
	UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * G_B2_3 = NULL;
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * G_B1_0 = NULL;
	UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * G_B1_1 = NULL;
	UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * G_B1_2 = NULL;
	UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * G_B1_3 = NULL;
	{
		// Debug.Log("click");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteralB93EC56608FB302FEA7A8019D7C0E8E7239FC033, /*hidden argument*/NULL);
		// UnityMessageManager.Instance.SendMessageToRN(new UnityMessage()
		// {
		//     name = "click",
		//     callBack = (data) =>
		//     {
		//         Debug.Log("onClickCallBack:" + data);
		//     }
		// });
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * L_0 = UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16_inline(/*hidden argument*/NULL);
		UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * L_1 = (UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 *)il2cpp_codegen_object_new(UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4_il2cpp_TypeInfo_var);
		UnityMessage__ctor_m76E68BAF631BE07953E806CA26C480BF23B2ED45(L_1, /*hidden argument*/NULL);
		UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * L_2 = L_1;
		NullCheck(L_2);
		L_2->set_name_0(_stringLiteralB93EC56608FB302FEA7A8019D7C0E8E7239FC033);
		UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * L_3 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D_il2cpp_TypeInfo_var);
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_4 = ((U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D_il2cpp_TypeInfo_var))->get_U3CU3E9__4_0_1();
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_5 = L_4;
		G_B1_0 = L_5;
		G_B1_1 = L_3;
		G_B1_2 = L_3;
		G_B1_3 = L_0;
		if (L_5)
		{
			G_B2_0 = L_5;
			G_B2_1 = L_3;
			G_B2_2 = L_3;
			G_B2_3 = L_0;
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D_il2cpp_TypeInfo_var);
		U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D * L_6 = ((U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_7 = (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)il2cpp_codegen_object_new(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0_il2cpp_TypeInfo_var);
		Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510(L_7, L_6, (intptr_t)((intptr_t)U3CU3Ec_U3COnMouseDownU3Eb__4_0_m174DA0084BF158C76305355492BE3C1FDDAAB0B1_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_RuntimeMethod_var);
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_8 = L_7;
		((U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D_il2cpp_TypeInfo_var))->set_U3CU3E9__4_0_1(L_8);
		G_B2_0 = L_8;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_003f:
	{
		NullCheck(G_B2_1);
		G_B2_1->set_callBack_2(G_B2_0);
		NullCheck(G_B2_3);
		UnityMessageManager_SendMessageToRN_m810379DB93ACA8FB4F37914CA49BA677BDE94E3E(G_B2_3, G_B2_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Rotate::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rotate_Update_m7EECA0DFF07C6BCA773478F66A949EE038D28330 (Rotate_t763544D6383D5853BC2A73F921EB5C3A7A2F836C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rotate_Update_m7EECA0DFF07C6BCA773478F66A949EE038D28330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// if (!canRotate)
		bool L_0 = __this->get_canRotate_4();
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// var delta = 30 * Time.deltaTime;
		float L_1 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		V_0 = ((float)il2cpp_codegen_multiply((float)(30.0f), (float)L_1));
		// transform.localRotation *= Quaternion.Euler(delta, delta, delta);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = L_2;
		NullCheck(L_3);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_4 = Transform_get_localRotation_mEDA319E1B42EF12A19A95AC0824345B6574863FE(L_3, /*hidden argument*/NULL);
		float L_5 = V_0;
		float L_6 = V_0;
		float L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_8 = Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05(L_5, L_6, L_7, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_9 = Quaternion_op_Multiply_mDB9F738AA8160E3D85549F4FEDA23BC658B5A790(L_4, L_8, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_3, L_9, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Rotate::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rotate__ctor_mC70374AA84BC2CA04CA039D1B56F5AF1B95D42CF (Rotate_t763544D6383D5853BC2A73F921EB5C3A7A2F836C * __this, const RuntimeMethod* method)
{
	{
		// private bool canRotate = true;
		__this->set_canRotate_4((bool)1);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Rotate_<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mDA82C2EF6063BB47B299296272C713873EB938C5 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_mDA82C2EF6063BB47B299296272C713873EB938C5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D * L_0 = (U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D *)il2cpp_codegen_object_new(U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mBFB4BD35AF855B60B3202619E831D4FB394CFC48(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Rotate_<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mBFB4BD35AF855B60B3202619E831D4FB394CFC48 (U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Rotate_<>c::<OnMouseDown>b__4_0(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3COnMouseDownU3Eb__4_0_m174DA0084BF158C76305355492BE3C1FDDAAB0B1 (U3CU3Ec_tD1CF31BB230950D06F81846D87A0C5958E56696D * __this, RuntimeObject * ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3COnMouseDownU3Eb__4_0_m174DA0084BF158C76305355492BE3C1FDDAAB0B1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("onClickCallBack:" + data);
		RuntimeObject * L_0 = ___data0;
		String_t* L_1 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral74B39E69E7652005F14B9B3ADE70D6C8DE94C051, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityMessage::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessage__ctor_m76E68BAF631BE07953E806CA26C480BF23B2ED45 (UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C void DEFAULT_CALL onUnityMessage(char*);
// System.Void UnityMessageManager::onUnityMessage(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_onUnityMessage_mC3031C4585567E720F536DB4EE28B0C20AB8A7C4 (String_t* ___message0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(onUnityMessage)(____message0_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

}
// System.Int32 UnityMessageManager::generateId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityMessageManager_generateId_m39045BEBA86C4467AD5549DFA0DDCDF9A7A9CB89 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityMessageManager_generateId_m39045BEBA86C4467AD5549DFA0DDCDF9A7A9CB89_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ID = ID + 1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		int32_t L_0 = ((UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_StaticFields*)il2cpp_codegen_static_fields_for(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var))->get_ID_5();
		((UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_StaticFields*)il2cpp_codegen_static_fields_for(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var))->set_ID_5(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1)));
		// return ID;
		int32_t L_1 = ((UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_StaticFields*)il2cpp_codegen_static_fields_for(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var))->get_ID_5();
		return L_1;
	}
}
// UnityMessageManager UnityMessageManager::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static UnityMessageManager Instance { get; private set; }
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * L_0 = ((UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_StaticFields*)il2cpp_codegen_static_fields_for(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void UnityMessageManager::set_Instance(UnityMessageManager)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_set_Instance_m25A5E4940172A7950BA2EED339C161A0C74B05AE (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityMessageManager_set_Instance_m25A5E4940172A7950BA2EED339C161A0C74B05AE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static UnityMessageManager Instance { get; private set; }
		UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		((UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_StaticFields*)il2cpp_codegen_static_fields_for(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void UnityMessageManager::add_OnMessage(UnityMessageManager_MessageDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_add_OnMessage_m7B623FDD1EB187BC9593C933D5E01766FC8DB344 (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * __this, MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityMessageManager_add_OnMessage_m7B623FDD1EB187BC9593C933D5E01766FC8DB344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * V_0 = NULL;
	MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * V_1 = NULL;
	MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * V_2 = NULL;
	{
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_0 = __this->get_OnMessage_7();
		V_0 = L_0;
	}

IL_0007:
	{
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_1 = V_0;
		V_1 = L_1;
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_2 = V_1;
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E *)CastclassSealed((RuntimeObject*)L_4, MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E_il2cpp_TypeInfo_var));
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E ** L_5 = __this->get_address_of_OnMessage_7();
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_6 = V_2;
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_7 = V_1;
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_8 = InterlockedCompareExchangeImpl<MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E *>((MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E **)L_5, L_6, L_7);
		V_0 = L_8;
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_9 = V_0;
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_10 = V_1;
		if ((!(((RuntimeObject*)(MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E *)L_9) == ((RuntimeObject*)(MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityMessageManager::remove_OnMessage(UnityMessageManager_MessageDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_remove_OnMessage_mE747C20C6838E183F2C60960D2C697D7997588F4 (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * __this, MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityMessageManager_remove_OnMessage_mE747C20C6838E183F2C60960D2C697D7997588F4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * V_0 = NULL;
	MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * V_1 = NULL;
	MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * V_2 = NULL;
	{
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_0 = __this->get_OnMessage_7();
		V_0 = L_0;
	}

IL_0007:
	{
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_1 = V_0;
		V_1 = L_1;
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_2 = V_1;
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E *)CastclassSealed((RuntimeObject*)L_4, MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E_il2cpp_TypeInfo_var));
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E ** L_5 = __this->get_address_of_OnMessage_7();
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_6 = V_2;
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_7 = V_1;
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_8 = InterlockedCompareExchangeImpl<MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E *>((MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E **)L_5, L_6, L_7);
		V_0 = L_8;
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_9 = V_0;
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_10 = V_1;
		if ((!(((RuntimeObject*)(MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E *)L_9) == ((RuntimeObject*)(MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityMessageManager::add_OnRNMessage(UnityMessageManager_MessageHandlerDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_add_OnRNMessage_mD1D6F59672DB48353711FBD7CC5A1C5573FF33FA (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * __this, MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityMessageManager_add_OnRNMessage_mD1D6F59672DB48353711FBD7CC5A1C5573FF33FA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * V_0 = NULL;
	MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * V_1 = NULL;
	MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * V_2 = NULL;
	{
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_0 = __this->get_OnRNMessage_8();
		V_0 = L_0;
	}

IL_0007:
	{
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_1 = V_0;
		V_1 = L_1;
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_2 = V_1;
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 *)CastclassSealed((RuntimeObject*)L_4, MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101_il2cpp_TypeInfo_var));
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 ** L_5 = __this->get_address_of_OnRNMessage_8();
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_6 = V_2;
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_7 = V_1;
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_8 = InterlockedCompareExchangeImpl<MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 *>((MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 **)L_5, L_6, L_7);
		V_0 = L_8;
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_9 = V_0;
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_10 = V_1;
		if ((!(((RuntimeObject*)(MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 *)L_9) == ((RuntimeObject*)(MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityMessageManager::remove_OnRNMessage(UnityMessageManager_MessageHandlerDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_remove_OnRNMessage_m12B8512E35DC119F3800C8C8A05442F9C0A19B5E (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * __this, MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityMessageManager_remove_OnRNMessage_m12B8512E35DC119F3800C8C8A05442F9C0A19B5E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * V_0 = NULL;
	MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * V_1 = NULL;
	MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * V_2 = NULL;
	{
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_0 = __this->get_OnRNMessage_8();
		V_0 = L_0;
	}

IL_0007:
	{
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_1 = V_0;
		V_1 = L_1;
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_2 = V_1;
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 *)CastclassSealed((RuntimeObject*)L_4, MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101_il2cpp_TypeInfo_var));
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 ** L_5 = __this->get_address_of_OnRNMessage_8();
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_6 = V_2;
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_7 = V_1;
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_8 = InterlockedCompareExchangeImpl<MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 *>((MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 **)L_5, L_6, L_7);
		V_0 = L_8;
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_9 = V_0;
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_10 = V_1;
		if ((!(((RuntimeObject*)(MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 *)L_9) == ((RuntimeObject*)(MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityMessageManager::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager__cctor_mDD0387729BD393CF289CB737BC70BF82CFAC885B (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityMessageManager__cctor_mDD0387729BD393CF289CB737BC70BF82CFAC885B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GameObject go = new GameObject("UnityMessageManager");
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686(L_0, _stringLiteralDF5E1AA3CE899D450ACCBFF1001276669BA2D6F1, /*hidden argument*/NULL);
		// DontDestroyOnLoad(go);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m4DC90770AD6084E4B1B8489C6B41205DC020C207(L_1, /*hidden argument*/NULL);
		// Instance = go.AddComponent<UnityMessageManager>();
		NullCheck(L_1);
		UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * L_2 = GameObject_AddComponent_TisUnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_mB6B969AE735C0C299C10DB75E705C75E6AFCEF47(L_1, /*hidden argument*/GameObject_AddComponent_TisUnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_mB6B969AE735C0C299C10DB75E705C75E6AFCEF47_RuntimeMethod_var);
		UnityMessageManager_set_Instance_m25A5E4940172A7950BA2EED339C161A0C74B05AE_inline(L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityMessageManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_Awake_mCBF7274D38E90447257210355EEBED184FF0C035 (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void UnityMessageManager::SendMessageToRN(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_SendMessageToRN_m7FA689191F69A7BD1CAF2218D61EE8BCE156B250 (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * __this, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityMessageManager_SendMessageToRN_m7FA689191F69A7BD1CAF2218D61EE8BCE156B250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// if (Application.platform == RuntimePlatform.Android)
		int32_t L_0 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0035;
		}
	}
	{
		// using (AndroidJavaClass jc = new AndroidJavaClass("no.asmadsen.unity.view.UnityUtils"))
		AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * L_1 = (AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE *)il2cpp_codegen_object_new(AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_mAE416E812DB3911279C0FE87A7760247CE1BBFA8(L_1, _stringLiteral9057C537F34973CF261CD8B9624E74700BB9737A, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		// jc.CallStatic("onUnityMessage", message);
		AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * L_2 = V_0;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = L_3;
		String_t* L_5 = ___message0;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		NullCheck(L_2);
		AndroidJavaObject_CallStatic_m12D78F5584C63F5D8B2344CBA8611EFBDDA669AF(L_2, _stringLiteralD3ED06D8ADCBDFF6079A520D21BA08F90A850BB2, L_4, /*hidden argument*/NULL);
		// }
		IL2CPP_LEAVE(0x53, FINALLY_002b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		{
			AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * L_6 = V_0;
			if (!L_6)
			{
				goto IL_0034;
			}
		}

IL_002e:
		{
			AndroidJavaClass_t799D386229C77D27C7E129BEF7A79AFD426084EE * L_7 = V_0;
			NullCheck(L_7);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_7);
		}

IL_0034:
		{
			IL2CPP_END_FINALLY(43)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x53, IL_0053)
	}

IL_0035:
	{
		// else if (Application.platform == RuntimePlatform.IPhonePlayer)
		int32_t L_8 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)8))))
		{
			goto IL_0053;
		}
	}
	{
		// Debug.Log("SendMessageToRN: " + message);
		String_t* L_9 = ___message0;
		String_t* L_10 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralB540547305C0BAB0BDAB2AD954B540FF4E01E999, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_10, /*hidden argument*/NULL);
		// onUnityMessage(message);
		String_t* L_11 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		UnityMessageManager_onUnityMessage_mC3031C4585567E720F536DB4EE28B0C20AB8A7C4(L_11, /*hidden argument*/NULL);
	}

IL_0053:
	{
		// }
		return;
	}
}
// System.Void UnityMessageManager::SendMessageToRN(UnityMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_SendMessageToRN_m810379DB93ACA8FB4F37914CA49BA677BDE94E3E (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * __this, UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityMessageManager_SendMessageToRN_m810379DB93ACA8FB4F37914CA49BA677BDE94E3E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * V_1 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B3_0 = 0;
	String_t* G_B5_0 = NULL;
	int32_t G_B5_1 = 0;
	{
		// int id = generateId();
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		int32_t L_0 = UnityMessageManager_generateId_m39045BEBA86C4467AD5549DFA0DDCDF9A7A9CB89(/*hidden argument*/NULL);
		V_0 = L_0;
		// if (message.callBack != null)
		UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * L_1 = ___message0;
		NullCheck(L_1);
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_2 = L_1->get_callBack_2();
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		// waitCallbackMessageMap.Add(id, message);
		Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 * L_3 = __this->get_waitCallbackMessageMap_9();
		int32_t L_4 = V_0;
		UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * L_5 = ___message0;
		NullCheck(L_3);
		Dictionary_2_Add_m93C9C887896A7CE63E5B6F2BE015A32C757FB41D(L_3, L_4, L_5, /*hidden argument*/Dictionary_2_Add_m93C9C887896A7CE63E5B6F2BE015A32C757FB41D_RuntimeMethod_var);
	}

IL_001b:
	{
		// JObject o = JObject.FromObject(new
		// {
		//     id = id,
		//     seq = message.callBack != null ? "start" : "",
		//     name = message.name,
		//     data = message.data
		// });
		int32_t L_6 = V_0;
		UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * L_7 = ___message0;
		NullCheck(L_7);
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_8 = L_7->get_callBack_2();
		G_B3_0 = L_6;
		if (L_8)
		{
			G_B4_0 = L_6;
			goto IL_002b;
		}
	}
	{
		G_B5_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		G_B5_1 = G_B3_0;
		goto IL_0030;
	}

IL_002b:
	{
		G_B5_0 = _stringLiteral2B020927D3C6EB407223A1BAA3D6CE3597A3F88D;
		G_B5_1 = G_B4_0;
	}

IL_0030:
	{
		UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * L_9 = ___message0;
		NullCheck(L_9);
		String_t* L_10 = L_9->get_name_0();
		UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * L_11 = ___message0;
		NullCheck(L_11);
		JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * L_12 = L_11->get_data_1();
		U3CU3Ef__AnonymousType1_4_t28B41C550A5824C1512F73D8BF89723305904D5B * L_13 = (U3CU3Ef__AnonymousType1_4_t28B41C550A5824C1512F73D8BF89723305904D5B *)il2cpp_codegen_object_new(U3CU3Ef__AnonymousType1_4_t28B41C550A5824C1512F73D8BF89723305904D5B_il2cpp_TypeInfo_var);
		U3CU3Ef__AnonymousType1_4__ctor_m253F43B5DCC70E3BE151417F11F720589BA93C3A(L_13, G_B5_1, G_B5_0, L_10, L_12, /*hidden argument*/U3CU3Ef__AnonymousType1_4__ctor_m253F43B5DCC70E3BE151417F11F720589BA93C3A_RuntimeMethod_var);
		JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * L_14 = JObject_FromObject_mF56A1A94F0D967B8EE58CD8A0C9BB7EDDDDD94E1(L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		// UnityMessageManager.Instance.SendMessageToRN(MessagePrefix + o.ToString());
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * L_15 = UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16_inline(/*hidden argument*/NULL);
		JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * L_16 = V_1;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		String_t* L_18 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral0C58A8D623E334F9AB068259F6E168424141F2C8, L_17, /*hidden argument*/NULL);
		NullCheck(L_15);
		UnityMessageManager_SendMessageToRN_m7FA689191F69A7BD1CAF2218D61EE8BCE156B250(L_15, L_18, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityMessageManager::onMessage(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_onMessage_mD1704644A461EFD311809BB58F82D36898713485 (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * __this, String_t* ___message0, const RuntimeMethod* method)
{
	{
		// if (OnMessage != null)
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_0 = __this->get_OnMessage_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		// OnMessage(message);
		MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * L_1 = __this->get_OnMessage_7();
		String_t* L_2 = ___message0;
		NullCheck(L_1);
		MessageDelegate_Invoke_m2D199E1633D3EFBEA727164B8A3F7888E6A4F801(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0014:
	{
		// }
		return;
	}
}
// System.Void UnityMessageManager::onRNMessage(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_onRNMessage_m47BA244DB7A9E9A2486007E2F5217A03FFFC1AF8 (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * __this, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityMessageManager_onRNMessage_m47BA244DB7A9E9A2486007E2F5217A03FFFC1AF8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * V_0 = NULL;
	UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * V_1 = NULL;
	{
		// if (message.StartsWith(MessagePrefix))
		String_t* L_0 = ___message0;
		NullCheck(L_0);
		bool L_1 = String_StartsWith_m7D468FB7C801D9C2DBEEEEC86F8BA8F4EC3243C1(L_0, _stringLiteral0C58A8D623E334F9AB068259F6E168424141F2C8, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		// message = message.Replace(MessagePrefix, "");
		String_t* L_2 = ___message0;
		NullCheck(L_2);
		String_t* L_3 = String_Replace_m970DFB0A280952FA7D3BA20AB7A8FB9F80CF6470(L_2, _stringLiteral0C58A8D623E334F9AB068259F6E168424141F2C8, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		___message0 = L_3;
		// }
		goto IL_0022;
	}

IL_0021:
	{
		// return;
		return;
	}

IL_0022:
	{
		// MessageHandler handler = MessageHandler.Deserialize(message);
		String_t* L_4 = ___message0;
		MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * L_5 = MessageHandler_Deserialize_mA9693BA8D2C9FD955FEA9DD8E48D55E6584BB8DE(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		// if ("end".Equals(handler.seq))
		MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = L_6->get_seq_1();
		NullCheck(_stringLiteral7A92F3D26362D6557D5701DE77A63A01DF61E57F);
		bool L_8 = String_Equals_m9C4D78DFA0979504FE31429B64A4C26DF48020D1(_stringLiteral7A92F3D26362D6557D5701DE77A63A01DF61E57F, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_007c;
		}
	}
	{
		// if (waitCallbackMessageMap.TryGetValue(handler.id, out m))
		Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 * L_9 = __this->get_waitCallbackMessageMap_9();
		MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_id_0();
		NullCheck(L_9);
		bool L_12 = Dictionary_2_TryGetValue_mE7EC42E3DBE2BF1335495A5EA5CB26B213784C41(L_9, L_11, (UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 **)(&V_1), /*hidden argument*/Dictionary_2_TryGetValue_mE7EC42E3DBE2BF1335495A5EA5CB26B213784C41_RuntimeMethod_var);
		if (!L_12)
		{
			goto IL_007b;
		}
	}
	{
		// waitCallbackMessageMap.Remove(handler.id);
		Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 * L_13 = __this->get_waitCallbackMessageMap_9();
		MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * L_14 = V_0;
		NullCheck(L_14);
		int32_t L_15 = L_14->get_id_0();
		NullCheck(L_13);
		Dictionary_2_Remove_m4B784310D2BB991FB6BC9036625E476B2A4BF958(L_13, L_15, /*hidden argument*/Dictionary_2_Remove_m4B784310D2BB991FB6BC9036625E476B2A4BF958_RuntimeMethod_var);
		// if (m.callBack != null)
		UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * L_16 = V_1;
		NullCheck(L_16);
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_17 = L_16->get_callBack_2();
		if (!L_17)
		{
			goto IL_007b;
		}
	}
	{
		// m.callBack(handler.getData<object>()); // todo
		UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * L_18 = V_1;
		NullCheck(L_18);
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_19 = L_18->get_callBack_2();
		MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * L_20 = V_0;
		NullCheck(L_20);
		RuntimeObject * L_21 = MessageHandler_getData_TisRuntimeObject_m7BDBAFA6583925FEFAA062BFF4909650CE5F28DF(L_20, /*hidden argument*/MessageHandler_getData_TisRuntimeObject_m7BDBAFA6583925FEFAA062BFF4909650CE5F28DF_RuntimeMethod_var);
		NullCheck(L_19);
		Action_1_Invoke_mB86FC1B303E77C41ED0E94FC3592A9CF8DA571D5(L_19, L_21, /*hidden argument*/Action_1_Invoke_mB86FC1B303E77C41ED0E94FC3592A9CF8DA571D5_RuntimeMethod_var);
	}

IL_007b:
	{
		// return;
		return;
	}

IL_007c:
	{
		// if (OnRNMessage != null)
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_22 = __this->get_OnRNMessage_8();
		if (!L_22)
		{
			goto IL_0090;
		}
	}
	{
		// OnRNMessage(handler);
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_23 = __this->get_OnRNMessage_8();
		MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * L_24 = V_0;
		NullCheck(L_23);
		MessageHandlerDelegate_Invoke_mF982647CDFA5782B9ABAF328FF4070492A364B43(L_23, L_24, /*hidden argument*/NULL);
	}

IL_0090:
	{
		// }
		return;
	}
}
// System.Void UnityMessageManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager__ctor_mFB3651A2237E489ED8746D5B8FC5B5AB95FE7CE7 (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityMessageManager__ctor_mFB3651A2237E489ED8746D5B8FC5B5AB95FE7CE7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private Dictionary<int, UnityMessage> waitCallbackMessageMap = new Dictionary<int, UnityMessage>();
		Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 * L_0 = (Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 *)il2cpp_codegen_object_new(Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mC61E608C684061D4497B7700BF059B5DDB97CE4B(L_0, /*hidden argument*/Dictionary_2__ctor_mC61E608C684061D4497B7700BF059B5DDB97CE4B_RuntimeMethod_var);
		__this->set_waitCallbackMessageMap_9(L_0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E (MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * __this, String_t* ___message0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	il2cppPInvokeFunc(____message0_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

}
// System.Void UnityMessageManager_MessageDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageDelegate__ctor_m3EA48B06536871149F1A43C905528166D49F48C8 (MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityMessageManager_MessageDelegate::Invoke(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageDelegate_Invoke_m2D199E1633D3EFBEA727164B8A3F7888E6A4F801 (MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * __this, String_t* ___message0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___message0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___message0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___message0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___message0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___message0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___message0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___message0, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< String_t* >::Invoke(targetMethod, targetThis, ___message0);
					else
						GenericVirtActionInvoker1< String_t* >::Invoke(targetMethod, targetThis, ___message0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___message0);
					else
						VirtActionInvoker1< String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___message0);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___message0) - 1), targetMethod);
				}
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___message0, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, String_t*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___message0, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult UnityMessageManager_MessageDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MessageDelegate_BeginInvoke_m89C256246D24A40498952C82CDD615B3507E4BA4 (MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * __this, String_t* ___message0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___message0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityMessageManager_MessageDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageDelegate_EndInvoke_m778326112BD3D26CB9DE0FFC335E9A1A9353BC35 (MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityMessageManager_MessageHandlerDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageHandlerDelegate__ctor_m58386047CB442F144728F0A568B26FDDDB0EE6EA (MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityMessageManager_MessageHandlerDelegate::Invoke(MessageHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageHandlerDelegate_Invoke_mF982647CDFA5782B9ABAF328FF4070492A364B43 (MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * __this, MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * ___handler0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___handler0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___handler0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___handler0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___handler0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___handler0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___handler0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___handler0, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * >::Invoke(targetMethod, targetThis, ___handler0);
					else
						GenericVirtActionInvoker1< MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * >::Invoke(targetMethod, targetThis, ___handler0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___handler0);
					else
						VirtActionInvoker1< MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___handler0);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___handler0) - 1), targetMethod);
				}
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___handler0, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___handler0, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult UnityMessageManager_MessageHandlerDelegate::BeginInvoke(MessageHandler,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MessageHandlerDelegate_BeginInvoke_mFDBB4C951CAC28A4F0E08666050BB3F266FA927E (MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * __this, MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * ___handler0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___handler0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityMessageManager_MessageHandlerDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageHandlerDelegate_EndInvoke_m9BC3850452E21DEBBF15C7598D811D4D744633A6 (MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16AssemblyU2DCSharp_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static UnityMessageManager Instance { get; private set; }
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * L_0 = ((UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_StaticFields*)il2cpp_codegen_static_fields_for(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_6();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void UnityMessageManager_set_Instance_m25A5E4940172A7950BA2EED339C161A0C74B05AE_inline (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityMessageManager_set_Instance_m25A5E4940172A7950BA2EED339C161A0C74B05AEAssemblyU2DCSharp_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static UnityMessageManager Instance { get; private set; }
		UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		((UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_StaticFields*)il2cpp_codegen_static_fields_for(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_6(L_0);
		return;
	}
}
